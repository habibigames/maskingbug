//
//  CCTextureCache+TextureCacheExtension.m
//  VERTEX
//
//  Created by Paul Hammer on 23.03.16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCTextureCache+TextureCacheExtension.h"

@implementation CCTextureCache (TextureCacheExtension)

- (CCTexture*) setTexturWithUImage: (UIImage*) img andKey: (NSString*) key {
    return [[CCTextureCache sharedTextureCache] addCGImage:[img CGImage] forKey:key];
}
//
//- (void) setTexture: (CCTexture*) tex forKey: (NSString*) key {
//    if ([self textureForKey:key]) [self removeTextureForKey:key];
//    [self textureForKey:<#(NSString *)#>]
//}

@end
