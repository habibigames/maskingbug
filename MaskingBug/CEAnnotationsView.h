//
//  CEAnnotationsView.h
//  VERTEX
//
//  Created by Paul Hammer on 13.07.16.
//  Copyright © 2016 ColossalEntertainment. All rights reserved.
//

#import "CCNode.h"
#import "CEMaskedContainer.h"
#import "CEAnnotationItem.h"

@interface CEAnnotationsView : CEMaskedContainer {
    NSMutableArray<CEAnnotationItem*> *_annotations;
}
+ (instancetype) annotationViewWithWidth:(float)w height:(float)h andCircleRect: (CGRect) _r;

- (void) generateNewAnnotation: (CEAnnotationItem_AnimationType) aT;
- (void) removeAnnotationForObject: (CEAnnotationItem*) _i;

@end
