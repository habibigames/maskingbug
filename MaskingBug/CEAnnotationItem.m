//
//  CEAnnotationItem.m
//  MaskingBug
//
//  Created by Paul Hammer on 17.07.16.
//  Copyright © 2016 Paul Willy Hammer. All rights reserved.
//

#import "CEAnnotationItem.h"

@implementation CEAnnotationItem

- (instancetype) initTextItem: (NSString*) _txt color: (CCColor*) _clr animation: (CEAnnotationItem_AnimationType) _aT {
    
    if (!self) self = [super init];
    
    clr     = _clr;
    text    = _txt;
    animType = _aT;
    
    
    [self generateAnnotation];
    
    return self;
    
}

- (void) generateAnnotation {
    
    //add the title
    CCLabelTTF *lbl = [CCLabelTTF labelWithString:text fontName:default_fontName fontSize:24];
    lbl.anchorPoint = ccp(0.5,0.0);
    
    //Get Random Position
    CGPoint loc = ccp(0, 0);
    
    lbl.positionInPoints = loc;
    
    [lbl setShadowBlurRadius:9.0f];
    [lbl setShadowColor:clr];
    [lbl setColorRGBA:clr];
    
//    CCRenderTexture *r = [CCRenderTexture renderTextureWithWidth:lbl.contentSizeInPoints.width height:lbl.contentSizeInPoints.height];
//    [r beginWithClear:0 g:0 b:0 a:0];
//    [lbl visit];
//    [r end];
    
//    s = [r sprite];
//    [s setAnchorPoint: lbl.anchorPoint];

//    CCRenderTexture *rend = [CCRenderTexture renderTextureWithWidth:lbl.contentSizeInPoints.width height:lbl.contentSizeInPoints.height];
//    [rend   beginWithClear:0 g:0 b:0 a:0];
//    [lbl    visit];
//    [rend   end];
//    
//    s = rend.sprite;
//    [s setAnchorPoint:ccp(0.5, 0.5)];
    
    s = lbl;
}

/**
 *  Call for starting Annotation animation
 */
- (void) startAnnotation {
    [[self getSprite] runAction:[self getAnimation]];
}

- (CCSprite*) getSprite {
    return s;
}

/**
 *  Creates an animation by the type of the animation
 *
 *  @return created animation
 */
- (CCAction*) getAnimation {
    const float fadeInDuration = 0.5;
    
    switch (animType) {
            
        case CEAT_1_1_1: {
            const float scaleBy = 1.5;
            const float scaleDuration = fadeInDuration * 2.0;
            
            return
            
        [CCActionSequence actions:
            [CCActionSpawn actions: //spawn start
             
             //Zoom
             [CCActionEaseInOut actionWithAction: [CCActionScaleBy actionWithDuration:scaleDuration scale:scaleBy] rate:2.0f],
             
             //Fading
             [CCActionSequence actions: //Sequence Start
              [CCActionFadeIn     actionWithDuration:fadeInDuration],
              [CCActionFadeOut    actionWithDuration:fadeInDuration]
              , nil], //Sequence End
             
             nil], //Spawn end
         
         [self getEndAction],
         
          nil]; //Seq End
        } break;
            
        case CEAT_1_1_2: {
            const float scaleByStart        = 1.4;
            const float scaleByEnd          = 0.6;
            const float animDurationStart   = 0.3;
            const float animDurationEnd     = 0.15;
            const float waitDuration        = 0.3;
            
            return
            
            //Fading
            [CCActionSequence actions: //Sequence Start
             
             //Zoom
             [CCActionSpawn actions:
              [CCActionEaseInOut actionWithAction:      [CCActionScaleBy actionWithDuration:animDurationStart scale:scaleByStart] rate:2.0],
              [CCActionFadeIn     actionWithDuration:   animDurationStart]
              , nil], // End
             
             [CCActionDelay actionWithDuration:waitDuration],
             
             [CCActionSpawn actions:
              [CCActionScaleBy actionWithDuration:      animDurationEnd scale:scaleByEnd],
              [CCActionFadeOut    actionWithDuration:   animDurationEnd]
              , nil], //Sequence End
             
             [self getEndAction],
             
             nil]; //Sequence end
        } break;
            
        case CEAT_1_1_3: {
            const float scaleByStart        = 1.4;
            const float scaleByEnd          = 0.6;
            const float animDurationStart   = 0.5;
            const float animDurationEnd     = 0.5;
//            const float waitDuration        = 0.3;
            
            return
            
            //Fading
            [CCActionSequence actions: //Sequence Start
             
             //Zoom
             [CCActionSpawn actions:
              [CCActionEaseInOut actionWithAction:      [CCActionScaleBy actionWithDuration:animDurationStart scale:scaleByStart] rate:2.0],
              [CCActionFadeIn     actionWithDuration:   animDurationStart]
              , nil], // End
             
//             [CCActionDelay actionWithDuration:waitDuration],
             
             [CCActionSpawn actions:
              [CCActionScaleBy actionWithDuration:      animDurationEnd scale:scaleByEnd],
              [CCActionFadeOut    actionWithDuration:   animDurationEnd]
              , nil], //Sequence End
             
             [self getEndAction],
             
             nil]; //Sequence end
        } break;
            
        case CEAT_1_2_1:{ //TODO NOCH ÄNDERN
           
        } break;
            
        case CEAT_2_1_1: {
            const float scaleByStart        = 1.4;
            const float scaleByEnd          = 0.6;
            const float animDurationStart   = 0.5;
            const float animDurationEnd     = 0.5;
            const float startRotation       = [CocosHelper getRndBetween:-30 and:-10];
            const float totalRotation       = [CocosHelper getRndBetween:270 and:360];
            
            return
            
            //Fading
            [CCActionSequence actions: //Sequence Start
             
             //Prepare for animation
             [CCActionCallBlock actionWithBlock:^{
                [self.getSprite setRotation:startRotation];
            }],
             
             //Zoom
             [CCActionSpawn actions:
//                  [CCActionEaseInOut actionWithAction:      [CCActionScaleBy actionWithDuration:animDurationStart scale:scaleByStart] rate:2.0],
                    [CCActionFadeIn     actionWithDuration:   animDurationStart],
                    [CCActionRotateBy   actionWithDuration:   animDurationStart  angle: totalRotation],
               nil], // End
             
             //             [CCActionDelay actionWithDuration:waitDuration],
             
             [CCActionSpawn actions:
//                  [CCActionScaleBy actionWithDuration:      animDurationEnd scale:scaleByEnd],
                    [CCActionFadeOut    actionWithDuration:   animDurationEnd]
              , nil], //Sequence End
             
             [self getEndAction],
             
             nil]; //Sequence end
        } break;
            
        case CEAT_2_1_2: {
            const float scaleByStart        = 1.4;
            const float scaleByEnd          = 0.6;
            const float animDurationStart   = 0.5;
            const float animDurationEnd     = 0.5;
            const float startRotation       = [CocosHelper getRndBetween:-30 and:-10];
            const float totalRotation       = [CocosHelper getRndBetween:270 and:360];
            
            return
            
            //Fading
            [CCActionSequence actions: //Sequence Start
             
             //Prepare for animation
             [CCActionCallBlock actionWithBlock:^{
                [self.getSprite setRotation:startRotation];
            }],
             
             //Zoom
             [CCActionSpawn actions:
              [CCActionEaseInOut actionWithAction:      [CCActionScaleBy actionWithDuration:animDurationStart scale:scaleByStart] rate:2.0],
              [CCActionFadeIn     actionWithDuration:   animDurationStart],
              [CCActionRotateBy   actionWithDuration:   animDurationStart  angle: totalRotation],
              nil], // End
             
             //             [CCActionDelay actionWithDuration:waitDuration],
             
             [CCActionSpawn actions:
              [CCActionScaleBy actionWithDuration:      animDurationEnd scale:scaleByEnd],
              [CCActionFadeOut    actionWithDuration:   animDurationEnd]
              , nil], //Sequence End
             
             [self getEndAction],
             
             nil]; //Sequence end
        } break;
            
        case CEAT_2_2_1:
        {
            NSMutableArray *spriteCopies = [NSMutableArray array];
            
            const float diffVar = 0.2f;
            const float endFadeOutDelay = 0.7;
            
            const float startRotation       = [CocosHelper getRndBetween: -30 and: -10];
            const float halfFadeInOpacityRotation = 120.0f;
            const float fullOpacityRotation = halfFadeInOpacityRotation * 2.0;
            const float endRotation         = [CocosHelper getRndBetween: 270.0f - fullOpacityRotation and: 360.0f - fullOpacityRotation];
            const float spriteScale         = [self.getSprite scale];
            
            CCActionCallBlock *copySpriteAction =
            [CCActionCallBlock actionWithBlock:^{
                CCSprite *sCopy =       [self.getSprite mutableCopy];
                [(CEAnnotationsView*)   self.getSprite.parent  addChild:sCopy];
                [spriteCopies           addObject:sCopy];
                
                //End Fade Out Action
                [sCopy runAction:
                 [CCActionSequence actions:
                    [CCActionFadeOut actionWithDuration:endFadeOutDelay],
                    [CCActionCallBlock actionWithBlock:^{
                     [spriteCopies      removeObject:sCopy];
                     [sCopy             removeFromParent];
                        }],
                   nil]
                 ];
            }];
            
            return [CCActionSequence actions:
                    
                    //Prepare for animation
                    [CCActionCallBlock actionWithBlock:^{
                        [self.getSprite setRotation:startRotation];
                        [self.getSprite setOpacity: 0.0f];
                    }],
                    
                    //Fade In half
                    [CCActionSpawn actions:
                     [CCActionEaseInOut actionWithAction: [CCActionRotateBy  actionWithDuration:diffVar angle:halfFadeInOpacityRotation] rate:2.0f],
                     [CCActionFadeTo    actionWithDuration:diffVar opacity:0.5f],
                     [CCActionEaseInOut actionWithAction: [CCActionScaleTo actionWithDuration:diffVar scale:self.getSprite.scale + 0.5] rate:2.0f],
                     nil],
                    copySpriteAction,
                    
                    //Fade Full In
                    [CCActionSpawn actions:
                     [CCActionEaseInOut actionWithAction:   [CCActionRotateBy  actionWithDuration:diffVar angle:fullOpacityRotation] rate:2.0f],
                     [CCActionFadeTo    actionWithDuration: diffVar opacity:1.0f],
                     [CCActionEaseInOut actionWithAction:   [CCActionScaleTo actionWithDuration:diffVar scale:spriteScale] rate:2.0f],
                     nil],
                    copySpriteAction,
                    
                    //Fade Out
                    [CCActionSpawn actions:
                     [CCActionEaseInOut actionWithAction:   [CCActionRotateBy  actionWithDuration:diffVar angle:endRotation] rate:2.0f],
                     [CCActionFadeTo    actionWithDuration: diffVar opacity:0.3f],
                     [CCActionEaseInOut actionWithAction:   [CCActionScaleTo actionWithDuration:diffVar scale:spriteScale - 0.5] rate:2.0f],
                     nil],
                    copySpriteAction,
                    
                    [self getEndAction],
                     nil];
        }
            break;

            
        default:
            return nil;
    }
    return nil;
}

/**
 *  After every animation this block will be called. It removes the sprite from it's parent and cleares the cache.
 */
- (CCAction*) getEndAction {
    return [CCActionCallBlock actionWithBlock:^{
        CEAnnotationsView   *_aIParent = (CEAnnotationsView *) self.getSprite.parent;
        [_aIParent          removeAnnotationForObject:self];
        [[CCTextureCache sharedTextureCache] removeTexture:s.texture];
    }];
}

@end
