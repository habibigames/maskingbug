//
//  CocosHelper.h
//  Colors
//
//  Created by Paul Hammer on 11.12.15.
//  Copyright © 2015 Paul Willy Hammer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#define degreesToRadians2( degrees )((degrees * M_PI) / 180)
#define radiansToDegrees2( radians )((radians * 180) / M_PI)
#define degreesToRadians( degrees )((degrees) / ( 180.0 * M_PI ))
#define radiansToDegrees( radians )((radians) * ( 180.0 / M_PI ))
#define  vectorLength(x,y)(sqrtf(powf(x,2)+powf(y,2)))
#define velocitySpeed(velocity)(sqrtf(powf(velocity.x,2)+powf(velocity.y,2)))
#define floatHalfSize(sz)(sz/2.0f)

@interface CocosHelper : NSObject

+ (void) addDefault_GradientBackground_ToLayer: (CCScene*) scene withZ:(int)z;
+ (CCNodeGradient*) addDefault_GradientBackground_ToLayer: (CCScene*) scene withOrder: (int) order;

#pragma mark Backgrounds
+ (CCNodeColor*) setup_Default_Background_ToLayer: (CCScene*) scene withOrder: (int) order;
+ (CCNodeColor*) setup_GameOver_Background_ToLayer: (CCScene*) scene withOrder: (int) order;

#pragma mark CircleSprite
+ (UIImage*)spriteRoundRect:(CGSize)size fill:(ccColor4B)fill outline:(ccColor4B)outline lineWidth:(float)lineWidth;
+ (CGPoint) getCenter: (CGSize)sz;

+ (void) preloadTextures;
+ (CCTexture*) getTextureWithKey: (NSString*) key;

+ (int) getRndBetween: (int) bottom and: (int) top;

#pragma mark shape
+ (UIImage*) createShapeWithSize: (CGSize)size radianDifference: (CGFloat)rdDiff outlineWidth: (CGFloat)oLWidth outlineColor:(UIColor*)outLineColor shadowWidth: (CGFloat) shdwWidth firstColor: (UIColor*) firstColor withFillValue: (float) firstColorFill andSecondColor:(UIColor*) secondColor withFillValue: (float) secondColorFill angle:(float)angle maxBrickAmount: (int) maxBrickAmount;

#pragma mark Circle
+ (UIImage*) createCircleWithSize: (CGSize)size radianDifference: (CGFloat)rdDiff outlineWidth: (CGFloat)oLWidth outlineColor:(UIColor*)outLineColor shadowWidth: (CGFloat) shdwWidth firstColor: (UIColor*) firstColor withFillValue: (float) firstColorFill andSecondColor:(UIColor*) secondColor withFillValue: (float) secondColorFill;
+ (UIImage*) createCircleWithSizeUsingCGColorRef: (CGSize)size radianDifference: (CGFloat)rdDiff outlineWidth: (CGFloat)oLWidth outlineColor:(CGColorRef)outLineColor shadowWidth: (CGFloat) shdwWidth firstColor: (CGColorRef) firstColor withFillValue: (float) firstColorFill andSecondColor:(CGColorRef) secondColor withFillValue: (float) secondColorFill;

#pragma mark collision handler
//Returns true if the circles are touching, or false if they are not
+ (bool) circlesCollidingPosition1: (CGPoint) pos1 radius1: (float) rd1 position2: (CGPoint) pos2 radius2:(float) rd2;

#pragma mark Color Handling
+ (UIColor*) colorForColorType: (int) cT;

#pragma mark Glow Effect for Sprites
+ (void)addGlowEffect:(CCSprite*)sprite color:(CCColor*)color size:(CGSize)size;

#pragma mark Convert methods
+ (UIImage *) renderUIImageFromSprite:  (CCSprite *)sprite;

#pragma mark Menu Circle Glow
+ (CCSprite *) createMenuInnerGlowCircleWithSize: (CGSize) sz;
+ (CCSprite *) createMenuOuterGlowCircleWithSize: (CGSize) sz;

#pragma mark Player Ball Glow
+ (CCSprite *) create_PlayerBall_OuterGlowCircleWithSize: (CGSize) sz;

@end
