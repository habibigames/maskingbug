//
//  CEAnnotationsView.m
//  VERTEX
//
//  Created by Paul Hammer on 13.07.16.
//  Copyright © 2016 ColossalEntertainment. All rights reserved.
//

#import "CEAnnotationsView.h"

static CCTexture *aMask;

@implementation CEAnnotationsView

+ (instancetype) annotationViewWithWidth:(float)w height:(float)h andCircleRect: (CGRect) _r {
    CEAnnotationsView *aV = [CEAnnotationsView containerWithWidth:w height:h];
    [aV setupWithCircleRect:_r]; //It creates a mask (ref's to. aMask)
    [aV setMask:aMask];
    return aV;
}

- (void) setupWithCircleRect:(CGRect)_r {
    if (CGRectEqualToRect(_r, CGRectZero)) NSLog(@"Could't setup annotation: setupWithCircleRect --> object _r (aka. CGRect) equals to nil!");
    if (!aMask) {
        //Get uniforms from PadControl Circle
        CGSize  pCrtl_sz    = _r.size;
        
        //Main ContentSize
        CGSize sz = self.contentSizeInPoints;
        
        float width     = sz.width;
        float height    = sz.height;
        
        //Opacities
        float cOpac     = 0.5;
        float bgOpac    = 0.8;
        
        //Colors
        UIColor *_UI_circleColor            = [UIColor colorWithWhite:1.0 alpha:cOpac];
        UIColor *_UI_backroundOpacityColor  = [UIColor colorWithWhite:1.0 alpha:bgOpac];
        
        //        CCColor *circleColor            = [CCColor colorWithUIColor:_UI_circleColor];
        CCColor *backroundOpacityColor  = [CCColor colorWithUIColor:_UI_backroundOpacityColor];
        
        //        UIImage *img2 = [img changeColorToColor:[UIColor blackColor]];
        //        UIImage *shdwImg = [self imageWithShadow:img BlurSize:blur alpha:alpha];
        //
        //        return [self makeAHoleIn:shdwImg withAMask:img2 onPosition:ccp((shdwImg.size.width-img2.size.width)/2, (shdwImg.size.height-img2.size.height)/2)];
        
        //Container Node for colored Nodes
        CCSprite *scissorRect   = [[CCSprite alloc] init];
        [scissorRect            setContentSizeInPoints:CGSizeMake(width, height)];
        
        //Create Image With an Hole in the middle
        CCNodeColor *clrNode = [CCNodeColor nodeWithColor:backroundOpacityColor width:width height:height];
        [clrNode        setAnchorPoint:ccp(0.5, 0.5)];
        [clrNode        setPositionType:CCPositionTypeNormalized];
        [clrNode        setPosition:ccp(0.5f, 0.5f)];
        [scissorRect    addChild:clrNode z:1];
        [clrNode        setColorRGBA:backroundOpacityColor]; // Set Color with alpha
        
        //Set AnchorPoint and Position of Container node
        [scissorRect    setAnchorPoint: ccp(0.0, 0.0)];
        [scissorRect    setPosition:    ccp(0, 0)];
        
        CCRenderTexture *rend   = [CCRenderTexture renderTextureWithWidth:width height:height];
        [rend beginWithClear:   0 g:0 b:0 a:0];
        [scissorRect            visit];
        [rend end];
        
        //        //Create a circle image
        //        UIImage *circleImg
        //                = [CocosHelper createCircleWithSize:pCrtl_sz radianDifference:pCrtl_sz.width/2.0f outlineWidth:0 outlineColor:[UIColor clearColor] shadowWidth:0 firstColor:_UI_circleColor withFillValue:cOpac andSecondColor:_UI_circleColor withFillValue:cOpac];
        
        
        //Create a circle image
        UIImage *circleImg_cutOut
        = [CocosHelper createCircleWithSize:pCrtl_sz radianDifference:pCrtl_sz.width/2.0f outlineWidth:0 outlineColor:[UIColor clearColor] shadowWidth:0 firstColor:[UIColor blackColor] withFillValue:1.0 andSecondColor:[UIColor blackColor] withFillValue:1.0];
        
        
        UIImage *circleImg_largeForMerge
        = [CocosHelper createCircleWithSize:CGSizeMake(pCrtl_sz.width*2.0, pCrtl_sz.height*2.0) radianDifference:pCrtl_sz.width outlineWidth:0 outlineColor:[UIColor clearColor] shadowWidth:0 firstColor:_UI_circleColor withFillValue:cOpac andSecondColor:_UI_circleColor withFillValue:cOpac];
        
        //create a background image by render texture
        UIImage *holeBG = [rend getUIImage];
        
        CGPoint cutOutPos   = ccp((holeBG.size.width-circleImg_cutOut.size.width)/2.0f, (holeBG.size.height-circleImg_cutOut.size.height)/2.0f);
        
        CGPoint cutOutPos_forLargeHole
        = ccp((holeBG.size.width-circleImg_largeForMerge.size.width/2), (holeBG.size.height-circleImg_largeForMerge.size.height/2));
        //
        holeBG          = [GlowManager makeAHoleIn: holeBG withAMask:   circleImg_cutOut onPosition:cutOutPos];
        
        holeBG          = [GlowManager mergeImage:  holeBG with:        circleImg_largeForMerge onPosition:
                           //                           ccp(holeBG.size.width, holeBG.size.height)
                           //                            ccp(holeBG.size.width*0.25, holeBG.size.height*0.25)
                           cutOutPos_forLargeHole
                           ];
        
        
        //comment out the following for displaying the mask
        //            CGRect rect     = self.boundingBox;
        //            [self.parent addChild: scissorRect];
        //            [scissorRect    setPosition:ccp(sz.width/2.0, sz.height/2.0)];
        
        //    return rend.texture;
        aMask = [[CCTextureCache sharedTextureCache] addCGImage:holeBG.CGImage
                                                         forKey:@"CEAnnotationsView_MaskImage"];
        
        //        [scissorRect removeFromParent];
        
        //        CCTextureCache *texC = [CCTextureCache sharedTextureCache];
        //        [texC removeTexture:[bgS texture]];
        //        [texC removeTexture:[cS texture]];
    }
}

#pragma mark annotation Item Handling
- (NSMutableArray*) annotations {
    if (!_annotations) _annotations = [NSMutableArray<CEAnnotationItem*> array];
    return _annotations;
}

- (void) addAnnotation: (CEAnnotationItem*) _i {
    [self.annotations addObject:    _i];
    [self addChild:                 [_i getSprite]];
    [_i startAnnotation];
}

- (void) removeAnnotationForObject: (CEAnnotationItem*) _i {
    [self.annotations   removeObject:_i];
    [_i.getSprite       removeFromParent];
    _i = nil;
}

- (void) generateNewAnnotation: (CEAnnotationItem_AnimationType) aT {
    CCColor *clr = [CCColor colorWithUIColor: [CocosHelper colorForColorType:[CocosHelper getRndBetween:0 and:5]]];
    CEAnnotationItem_AnimationType animT = aT;
    
    CEAnnotationItem *aI    = [[CEAnnotationItem alloc] initTextItem:@"Test annotation" color:clr animation:animT];
    
    [[aI getSprite] setPositionInPoints:    [self makePositionForAnnotionItem:aI]];
    [self addAnnotation:    aI];
}

- (CGPoint) makePositionForAnnotionItem: (CEAnnotationItem*) _aI {
    CGPoint loc = ccp( [CocosHelper getRndBetween:_aI.getSprite.contentSizeInPoints.width/2.0 and:self.contentSizeInPoints.width - _aI.getSprite.contentSizeInPoints.width/2.0] , [CocosHelper getRndBetween:_aI.getSprite.contentSizeInPoints.height/2.0 and:self.contentSizeInPoints.height - _aI.getSprite.contentSizeInPoints.height/2.0]);
    
    return loc;
}

@end