//
//  CCButton_OptionsButton.h
//  VERTEX
//
//  Created by Paul Hammer on 22.08.16.
//  Copyright © 2016 ColossalEntertainment. All rights reserved.
//

#import "CCButton.h"

@interface CCToggleButton_OptionsButton : CCButton {
    id  __target;
    SEL __selector;
}
+ (CCToggleButton_OptionsButton*) buttonWithStatusEnabled: (BOOL) _e onTargetClass: (id) _t andSelectorAction: (SEL) _s preferedSizeInPoints: (CGSize) _sz;
- (BOOL) toggleStatus;

@end
