#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D u_MaskTexture;
//uniform vec2 u_MaskTextureSize; // content size in points
//uniform vec2 u_MaskTexturePixelSize; // content size in pixels

void main()
{
//    vec2 newPos         = cc_FragTexCoord1 + vec2(0.5,0.5);
    vec4 normalColor    = texture2D(cc_MainTexture, cc_FragTexCoord1);
    vec4 maskColor      = texture2D(u_MaskTexture, cc_FragTexCoord1);
    float alpha         = maskColor.a;
    
    gl_FragColor        = normalColor * alpha;
//    texture2D(CC_Texture0, fract(vec2(v_texCoord.x + offset, v_texCoord.y))).rgba;
}

//void main()
//{
//    //    vec2 newPos         = cc_FragTexCoord1 + vec2(0.5,0.5);
//    vec4 normalColor    = texture2D(cc_MainTexture, cc_FragTexCoord1);
//    vec4 maskColor      = texture2D(u_MaskTexture, cc_FragTexCoord1);
//    float alpha         = maskColor.a;
//    
//    
//    
//    
//    gl_FragColor        = normalColor * alpha;
//    //    texture2D(CC_Texture0, fract(vec2(v_texCoord.x + offset, v_texCoord.y))).rgba;
//}
//
//void main() {
////    vec3 color = vec3(0.0);
//    
//    vec4 normalColor    = texture2D(cc_MainTexture, cc_FragTexCoord1);
//    vec4 maskColor      = texture2D(u_MaskTexture, cc_FragTexCoord1);
//    float alpha         = maskColor.a;
//
//    vec2 st = cc_FragTexCoord1.xy;
//    vec4 color = normalColor;
//    
//    // move space from the center to the vec2(0.0)
////    st -= vec2(0.5);
////    // rotate the space
////    st = rotate2d( sin(u_time)*PI ) * st;
////    // move it back to the original place
////    st += vec2(0.5);
//    
////    // Add the shape on the foreground
////    color += vec3(cross(st,0.25));
//    
//    color = color * alpha;
//    
//    color += vec4(0.0,0.0,0.5,0.5);
//    
//    gl_FragColor = color * alpha;
//}