//
//  CCEffectNode_FadedBorder_CE.h
//  MaskingBug
//
//  Created by Paul Hammer on 23.08.16.
//  Copyright © 2016 Paul Willy Hammer. All rights reserved.
//

#import "CCEffectNode.h"

@interface CCEffectNode_FadedBorder_CE : CCEffectNode {
    CCTexture *mask;
}

+ (instancetype) containerWithWidth:(float)w height:(float)h;
- (void) setMask: (CCTexture*) _m;
+ (CCTexture*) createTransparencyTextureMaskWithSizeee: (CGSize) sz fadeEffectOnBottom: (BOOL) onBottom onTop: (BOOL) onTop;

@end
