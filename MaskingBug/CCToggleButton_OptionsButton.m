//
//  CCButton_OptionsButton.m
//  VERTEX
//
//  Created by Paul Hammer on 22.08.16.
//  Copyright © 2016 ColossalEntertainment. All rights reserved.
//

#import "CCToggleButton_OptionsButton.h"

@implementation CCToggleButton_OptionsButton {
    
}

+ (CCToggleButton_OptionsButton*) buttonWithStatusEnabled: (BOOL) _e onTargetClass: (id) _t andSelectorAction: (SEL) _s preferedSizeInPoints: (CGSize) _sz {
    CCSpriteFrame *h_f = [CCSpriteFrame frameWithImageNamed:switchButton_On];
    CCSpriteFrame *n_f = [CCSpriteFrame frameWithImageNamed:switchButton_Off];
    CCToggleButton_OptionsButton *btn = [CCToggleButton_OptionsButton buttonWithTitle:@"" spriteFrame:h_f highlightedSpriteFrame:n_f  //mabye change frames arrangement
                                                                  disabledSpriteFrame:nil];
//    [[btn ] setScale:_sz.width/[h_f texture].contentSize.width];
    [btn setAnchorPoint:ccp(0.5f, 0.5f)];
//    [btn setPreferredSize:_sz];
//    [btn setMaxSize:CGSizeMake(_sz.width+3.0, _sz.height+3.0)];
    [btn setScale:_sz.width/btn.contentSizeInPoints.width];
    [btn setTogglesSelectedState:TRUE];
    [btn setSelected:_e];
    [btn setTarget:btn selector:@selector(switchValue)];
    [btn setSwitchValueTarget: _t andSelector: _s];
    return btn;
}

- (void) setSwitchValueTarget: (id) _t andSelector: (SEL) _s {
    __target    = _t;
    __selector  = _s;
}

- (void) switchValue {
    if ([__target respondsToSelector:__selector] && __target) {
        [(CCNode*)__target runAction:[CCActionCallFunc actionWithTarget:__target selector:__selector]];
    }
}

- (BOOL) toggleStatus {
    return self.selected;
}

@end