//
//  GlowManager.m
//  VERTEX
//
//  Created by Paul Hammer on 23.03.16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "GlowManager.h"
#import "UIImage+Tint.h"

@implementation GlowManager

+ (UIImage *)maskWithPaths:(NSArray *)paths bounds:(CGRect)bounds
{
    // Get the scale for good results on Retina screens.
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize scaledSize = CGSizeMake(bounds.size.width * scale, bounds.size.height * scale);
    
    // Create the bitmap with just an alpha channel.
    // When created, it has value 0 at every pixel.
    CGContextRef gc = CGBitmapContextCreate(NULL, scaledSize.width, scaledSize.height, 8, scaledSize.width, NULL, kCGImageAlphaOnly);
    
    // Adjust the current transform matrix for the screen scale.
    CGContextScaleCTM(gc, scale, scale);
    // Adjust the CTM in case the bounds origin isn't zero.
    CGContextTranslateCTM(gc, -bounds.origin.x, -bounds.origin.y);
    
    // whiteColor has all components 1, including alpha.
    CGContextSetFillColorWithColor(gc, [UIColor whiteColor].CGColor);
    
    // Fill each path into the mask.
    for (UIBezierPath *path in paths) {
        CGContextBeginPath(gc);
        CGContextAddPath(gc, path.CGPath);
        CGContextFillPath(gc);
    }
    
    // Turn the bitmap context into a UIImage.
    CGImageRef cgImage = CGBitmapContextCreateImage(gc);
    CGContextRelease(gc);
    UIImage *image = [UIImage imageWithCGImage:cgImage scale:scale orientation:UIImageOrientationDownMirrored];
    CGImageRelease(cgImage);
    return image;
}

+ (UIImage *)invertedImageWithMask:(UIImage *)mask color:(UIColor *)color
{
    CGRect rect = { CGPointZero, mask.size };
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, mask.scale); {
        // Fill the entire image with color.
        [color setFill];
        UIRectFill(rect);
        // Now erase the masked part.
        CGContextClipToMask(UIGraphicsGetCurrentContext(), rect, mask.CGImage);
        CGContextClearRect(UIGraphicsGetCurrentContext(), rect);
    }
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)invertedImageWithMask2:(UIImage *)mask color:(UIColor *)color
{
    CGRect rect = { CGPointZero, mask.size };
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, mask.scale); {
        // Fill the entire image with color.
        [color setFill];
        UIRectFill(rect);
//        // Now erase the masked part.
        CGContextClipToMask(UIGraphicsGetCurrentContext(), rect, mask.CGImage);
        CGContextClearRect(UIGraphicsGetCurrentContext(), rect);
    }
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (void)drawInnerGlowWithPaths:(NSArray *)paths bounds:(CGRect)bounds color:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur
{
    UIImage *mask = [self maskWithPaths:paths bounds:bounds];
    UIImage *invertedImage = [self invertedImageWithMask:mask color:color];
    CGContextRef gc = UIGraphicsGetCurrentContext();
    
    // Save the graphics state so I can restore the clip and
    // shadow attributes after drawing.
    CGContextSaveGState(gc); {
        CGContextClipToMask(gc, bounds, mask.CGImage);
        CGContextSetShadowWithColor(gc, offset, blur, color.CGColor);
        [invertedImage drawInRect:bounds];
    } CGContextRestoreGState(gc);
}

+ (UIImage*)drawInnerGlowWithMaskImage: (UIImage*) mask bounds:(CGRect)bounds color:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur
{
    UIImage *invertedImage = [self invertedImageWithMask:mask color:color];
    
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, [UIScreen mainScreen].scale);
    
    CGContextRef gc = UIGraphicsGetCurrentContext();

    // Save the graphics state so I can restore the clip and
    // shadow attributes after drawing.
    CGContextSaveGState(gc); {
        CGContextClipToMask(gc, bounds, mask.CGImage);
        CGContextSetShadowWithColor(gc, offset, blur, color.CGColor);
        [invertedImage drawAtPoint:CGPointZero];
    } CGContextRestoreGState(gc);
        
        UIImage *imgGlow = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    return imgGlow;
}

+ (void) drawGlowWithImage2:(UIImage *)img bounds:(CGRect)bounds color:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur
{
    UIImage *invertedImage = [self invertedImageWithMask2:img color:color];
//        UIImage *invertedImage = [self maskImageFromImageAlphaByImage:img];
    CGContextRef gc = UIGraphicsGetCurrentContext();
    
    // Save the graphics state so I can restore the clip and
    // shadow attributes after drawing.
    CGContextSaveGState(gc); {
                CGContextClipToMask(gc, bounds, img.CGImage);
        CGContextSetShadowWithColor(gc, offset, blur, color.CGColor);
        [invertedImage drawInRect:bounds];
    } CGContextRestoreGState(gc);
}

+ (void) drawGlowWithImage:(UIImage *)img bounds:(CGRect)bounds color:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur
{
    UIImage *invertedImage = [self invertedImageWithMask2:img color:color];
//    UIImage *invertedImage = [self maskImageFromImageAlphaByImage:img];
    CGContextRef gc = UIGraphicsGetCurrentContext();
    
    // Save the graphics state so I can restore the clip and
    // shadow attributes after drawing.
    CGContextSaveGState(gc); {
//        CGContextClipToMask(gc, bounds, img.CGImage);
        CGContextSetShadowWithColor(gc, offset, blur, color.CGColor);
        [invertedImage drawInRect:bounds];
    } CGContextRestoreGState(gc);
}

+ (UIImage*)generateGlowImageByImage:(UIImage*) img hue: (CGFloat) hue saturation:(CGFloat)saturation brihgtness: (CGFloat) brightness alpha: (CGFloat) alpha offset:(CGSize)offset blur:(CGFloat)blur
{
//    UIBezierPath *path1 = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(20, 20, 60, 60)];
//    UIBezierPath *path2 = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(50, 50, 60, 60)];
//    NSArray *paths = [NSArray arrayWithObjects:path1, path2, nil];

    UIGraphicsBeginImageContextWithOptions(img.size, NO, 0.0); {
//        [self drawInnerGlowWithPaths:paths bounds:CGRectMake(0, 0, img.size.width, img.size.height) color:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:alpha] offset:offset blur:blur];
        [self drawGlowWithImage:img bounds:CGRectMake(0, 0, img.size.width, img.size.height) color:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:alpha]
                         offset:offset blur:blur];
//        [self drawInnerGlowWithPaths:paths bounds:CGRectMake(0, 0, img.size.width, img.size.height) color:[UIColor colorWithHue:0 saturation:1 brightness:.8 alpha:.8] offset:CGSizeZero blur:10.0];
    }
    UIImage *imgGlow = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imgGlow;
}


+ (UIImage*) createInnerGlowImageByImage:(UIImage *)img alpha:(CGFloat)alpha offset:(CGSize)offset blur:(CGFloat)blur {
//    UIImage *img2 = [img changeColorToColor:[UIColor blackColor]];
//    UIImage *shdwImg = [self imageWithShadow:img BlurSize:blur alpha:alpha];
//    img2 = [self invertImage:img2];
//    UIImage *shdwImg = [self drawInnerGlowWithMaskImage:img bounds:CGRectMake(0, 0, img.size.width, img.size.height) color:[UIColor whiteColor] offset:offset blur:blur];
//    return shdwImg;
//    return [self makeAHoleIn:shdwImg withAMask:img2 onPosition:ccp((shdwImg.size.width-img2.size.width)/2, (shdwImg.size.height-img2.size.height)/2)];
    
    
}

+ (UIImage*) createOuterGlowImageByImage:(UIImage *)img alpha:(CGFloat)alpha offset:(CGSize)offset blur:(CGFloat)blur {
    UIImage *img2 = [img changeColorToColor:[UIColor blackColor]];
    UIImage *shdwImg = [self imageWithShadow:img BlurSize:blur alpha:alpha];
    
    return [self makeAHoleIn:shdwImg withAMask:img2 onPosition:ccp((shdwImg.size.width-img2.size.width)/2, (shdwImg.size.height-img2.size.height)/2)];
}

+ (UIImage*) createOuterGlowImageByImage_withoutCuttingOutOriginalImage:(UIImage *)img alpha:(CGFloat)alpha offset:(CGSize)offset blur:(CGFloat)blur {
    UIImage *img2 = [img changeColorToColor:[UIColor blackColor]];
    UIImage *shdwImg = [self imageWithShadow:img BlurSize:blur alpha:alpha];
    
    return shdwImg;
//    [self makeAHoleIn:shdwImg withAMask:img2 onPosition:ccp((shdwImg.size.width-img2.size.width)/2, (shdwImg.size.height-img2.size.height)/2)];
}

+ (UIImage*) createGlowImageByImage:(UIImage *)img alpha:(CGFloat)alpha offset:(CGSize)offset blur:(CGFloat)blur {
//    UIImage */*img2 = [self getImageWithUnsaturatedPixelsOfImage:img];
//    img2 */ img2 = [self getImageWithTintedColor:/*img2*/img withTint:[UIColor blackColor] withIntensity:1.0];
//    UIImage *img2 = [img imageTintedWithColor:[UIColor whiteColor]];
//    UIImage *shdw = [self addShdwForImg:img BlurSize:30 alpha:0.8];
    
    
    
    UIImage *img2 = [img changeColorToColor:[UIColor blackColor]];
    UIImage *shdwImg = [self imageWithShadow:img BlurSize:blur alpha:alpha];
    
    return [self makeAHoleIn:shdwImg withAMask:img2 onPosition:ccp((shdwImg.size.width-img2.size.width)/2, (shdwImg.size.height-img2.size.height)/2)];
//    return [self makeAHoleIn:shdw withAMask:img2 onPosition:CGPointZero];
    //    return [self imageWithShadowColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:alpha] offset:offset blur:blur byImage:img];
}

+ (UIImage*)imageWithShadow:(UIImage*)originalImage BlurSize:(float)blurSize alpha: (float) alpha {
    CGImageRef shadowedCGImage;
    
    UIGraphicsBeginImageContextWithOptions(originalImage.size, NO, [UIScreen mainScreen].scale);
    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef shadowContext = CGBitmapContextCreate(NULL, originalImage.size.width + (blurSize*2), originalImage.size.height + (blurSize*2), CGImageGetBitsPerComponent(originalImage.CGImage), 0, colourSpace, kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colourSpace);
    CGContextSetShouldAntialias(shadowContext, YES); //ADDED 31.03.2016 15:11 Uhr
    CGContextSetShadowWithColor(shadowContext, CGSizeMake(0, 0), blurSize, [UIColor colorWithWhite:1.0f alpha:alpha].CGColor);
    CGContextDrawImage(shadowContext, CGRectMake(blurSize, blurSize, originalImage.size.width, originalImage.size.height), originalImage.CGImage);
    
    shadowedCGImage = CGBitmapContextCreateImage(shadowContext);

//    CGContextRelease(shadowContext);
    
    UIImage * shadowedImage = [UIImage imageWithCGImage:shadowedCGImage];
    CGContextRelease(shadowContext);

//    CGImageRelease(shadowedCGImage);
    UIGraphicsEndImageContext();
//
    return shadowedImage;
//    return [self imageByCutOutMask:[self invertedImageWithMask:originalImage color:[UIColor blackColor]] fromImage: shadowedImage];
}

//+ (UIImage*)shadowImageByImage: (UIImage*) img and
//{
//    [super drawRect:rect];
//    
//    rect = [self bounds];
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSaveGState(context);
//    
//    // use the same background color as the view, but with an alpha of 1.0
//    const CGFloat * colorComps =
//    CGColorGetComponents(self.backgroundColor.CGColor);
//    CGContextSetRGBFillColor(context, colorComps[0], colorComps[1],
//                             colorComps[2], 1.0);
//    
//    // set the shadow
//    CGContextSetShadow(context, CGSizeMake(0.0, -5.0), 10.0);
//    
//    CGRect rectToDraw = CGRectMake(
//                                   rect.origin.x,
//                                   rect.origin.y,
//                                   rect.size.width,
//                                   rect.size.height - 30.0);
//    
//    // draw the rect, including the shadow
//    CGContextFillRect(context, rectToDraw);
//    
//    CGContextRestoreGState(context);
//}

+ (UIImage *)imageWithShadowColor:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur byImage:(UIImage*) img
{
    UIImage *maskImg = [self maskImageFromImageAlphaByImage:img];
    CGRect bounds = CGRectMake(0, 0, img.size.width, img.size.height);
    
    //get size
    CGSize border = CGSizeMake(fabsf((float)offset.width) + blur, fabsf((float)offset.height) + blur);
    CGSize size = CGSizeMake(img.size.width + border.width * 2.0f, img.size.height + border.height * 2.0f);
    
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //set up shadow
    CGContextSetShadowWithColor(context, offset, blur, color.CGColor);
    CGContextClearRect(UIGraphicsGetCurrentContext(), bounds);
    
    //draw with shadow
    [maskImg drawInRect:bounds];
    
    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    
    //return image
    return [self imageByCutOutMask:img fromImage:image];
}

+ (UIImage *)maskImageFromImageAlphaByImage: (UIImage*) img
{
    //get dimensions
    NSInteger width = CGImageGetWidth(img.CGImage);
    NSInteger height = CGImageGetHeight(img.CGImage);
    
    //create alpha image
    NSInteger bytesPerRow = ((width + 3) / 4) * 4;
    uint8_t *data = (uint8_t *)malloc(bytesPerRow * height);
    CGContextRef context = CGBitmapContextCreate(data, width, height, 8, bytesPerRow, NULL, (CGBitmapInfo)kCGImageAlphaOnly);
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), img.CGImage);
    
    //invert alpha pixels
    for (NSInteger y = 0; y < height; y++)
    {
        for (NSInteger x = 0; x < width; x++)
        {
            NSInteger index = y * bytesPerRow + x;
            data[index] = 255 - data[index];
        }
    }
    
    //create mask image
    CGImageRef maskRef = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    UIImage *mask = [UIImage imageWithCGImage:maskRef];
    CGImageRelease(maskRef);
    free(data);
    
    //return image
    return mask;
}

+ (UIImage*) imageByCutOutMask: (UIImage*) mask fromImage: (UIImage*) img {
    CGRect bounds = CGRectMake(0, 0, img.size.width, img.size.height);
    
    UIGraphicsBeginImageContextWithOptions(img.size, NO, img.scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(ctx, 0.0, img.size.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    
    CGImageRef maskImage = mask.CGImage;
    CGContextClipToMask(ctx, bounds, maskImage);
    
    CGContextTranslateCTM(ctx, 0.0, img.size.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    
    [img drawInRect:bounds];
    
    UIImage *newImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();

    return newImg;
}


#pragma mark and invert image
+ (UIImage*) addShdwForImg: (UIImage*) img BlurSize:(float)blurSize alpha: (float) alpha {
    UIGraphicsBeginImageContext(img.size);
    CGContextRef myContext =  UIGraphicsGetCurrentContext();
    CGSize          myShadowOffset = CGSizeMake (5,  10);// 2
    float          myColorValues[] = {0, 0, 0, .3};// 3
    CGColorRef      myColor;// 4
    CGColorSpaceRef myColorSpace;// 5
    CGImageRef imgRef = img.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGContextSaveGState(myContext);// 6
    
    CGContextSetShadow (myContext, myShadowOffset, 5); // 7
    // Your drawing code here// 8
    CGContextSetRGBFillColor (myContext, 0, 1, 0, 1);
    // CGContextDrawImage(myContext, CGRectMake(0, 0, width, height), imgRef);
    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    myColorSpace = CGColorSpaceCreateDeviceRGB ();// 9
    myColor = [UIColor whiteColor].CGColor;// 10
    CGContextSetShadowWithColor (myContext, myShadowOffset, 5, myColor);// 11
    // Your drawing code here// 12
    CGContextSetRGBFillColor (myContext, 0, 0, 1, 1);
    //CGContextDrawImage(myContext, CGRectMake (width/3-75,height/2-100,width/4,height/4),imgRef);
    [img drawInRect:CGRectMake (width/3-75,height/2-100,width/4,height/4)];
    CGColorRelease (myColor);// 13
    CGColorSpaceRelease (myColorSpace); // 14
    
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    CGContextRestoreGState(myContext);
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

+ (UIImage *)createWhiteImageWithSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), 255, 255, 255, 1);
    CGContextFillRect (UIGraphicsGetCurrentContext(), CGRectMake (0, 0, size.width, size.height));
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext(); // <--
    UIGraphicsEndImageContext();
    return resultImage; // <--
}
+ (UIImage *) mergeImage:(UIImage *)backImage with:(UIImage *)frontImage onPosition:(CGPoint)position
{
    CGSize finalSize = backImage.size;
    CGSize impositionSize = frontImage.size;
//    impositionSize.width *= 2.0;
//    impositionSize.height *= 2.0;
    UIGraphicsBeginImageContextWithOptions(finalSize, NO, 0.0);
    [backImage drawInRect:CGRectMake(0, 0, finalSize.width, finalSize.height)];
//    [frontImage drawInRect:CGRectMake(0, 0, impositionSize.width, impositionSize.height)];
    [frontImage drawInRect:CGRectMake(position.x, position.y, impositionSize.width, impositionSize.height)];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext(); // <--
    UIGraphicsEndImageContext();
    return resultImage; // <--
}

+ (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage
{
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    UIImage *img = [UIImage imageWithCGImage:masked];
    CGImageRelease(mask); // <--
    CGImageRelease(masked); // <--
    return img;
}
+ (UIImage *)invertImage:(UIImage *)originalImage
{
    UIGraphicsBeginImageContextWithOptions(originalImage.size, NO, [UIScreen mainScreen].scale);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeCopy);
    [originalImage drawInRect:CGRectMake(0, 0, originalImage.size.width, originalImage.size.height)];
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeDifference);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(),[UIColor whiteColor].CGColor);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, originalImage.size.width, originalImage.size.height));
    UIImage *returnImage = UIGraphicsGetImageFromCurrentImageContext(); // <--
                            UIGraphicsEndImageContext();
                            return returnImage; // <--
}

+ (UIImage *)makeAHoleIn:(UIImage *)baseImage withAMask:(UIImage *)mask onPosition:(CGPoint)position
{
    UIImage *whiteImage = [self createWhiteImageWithSize:baseImage.size];
    UIImage *goodmask = [self mergeImage:whiteImage with:mask onPosition:position];
    UIImage *finalmask = [self invertImage:goodmask];
//    UIImage *finalmask = goodmask;
    UIImage *finalImage = [self maskImage:baseImage withMask:finalmask];
    return finalImage;
}

+(UIImage *) getImageWithUnsaturatedPixelsOfImage:(UIImage *)image {
    const int RED = 1, GREEN = 2, BLUE = 3;
    
    CGRect imageRect = CGRectMake(0, 0, image.size.width*2, image.size.height*2);
    
    int width = imageRect.size.width, height = imageRect.size.height;
    
    uint32_t * pixels = (uint32_t *) malloc(width*height*sizeof(uint32_t));
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [image CGImage]);
    
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t * rgbaPixel = (uint8_t *) &pixels[y*width+x];
            uint32_t gray = (0.3*rgbaPixel[RED]+0.59*rgbaPixel[GREEN]+0.11*rgbaPixel[BLUE]);
            
            rgbaPixel[RED] = gray;
            rgbaPixel[GREEN] = gray;
            rgbaPixel[BLUE] = gray;
        }
    }
    
    CGImageRef newImage = CGBitmapContextCreateImage(context);
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    UIImage * resultUIImage = [UIImage imageWithCGImage:newImage scale:2 orientation:0];
    CGImageRelease(newImage);
    
    return resultUIImage;
}

+(UIImage *) getImageWithTintedColor:(UIImage *)image withTint:(UIColor *)color withIntensity:(float)alpha {
    CGSize size = image.size;
    
    UIGraphicsBeginImageContextWithOptions(size, FALSE, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [image drawAtPoint:CGPointZero blendMode:kCGBlendModeNormal alpha:1.0];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextSetBlendMode(context, kCGBlendModeOverlay);
    CGContextSetAlpha(context, alpha);
    
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(CGPointZero.x, CGPointZero.y, image.size.width, image.size.height));
    
    UIImage * tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}


//    - (void)drawInnerShadowInContext:(CGContextRef)context
//withPath:(CGPathRef)path
//shadowColor:(CGColorRef)shadowColor
//offset:(CGSize)offset
//blurRadius:(CGFloat)blurRadius {
//    CGContextSaveGState(context);
//    
//    CGContextAddPath(context, path);
//    CGContextClip(context);
//    
//    CGColorRef opaqueShadowColor = CGColorCreateCopyWithAlpha(shadowColor, 1.0);
//    
//    CGContextSetAlpha(context, CGColorGetAlpha(shadowColor));
//    CGContextBeginTransparencyLayer(context, NULL);
//    CGContextSetShadowWithColor(context, offset, blurRadius, opaqueShadowColor);
//    CGContextSetBlendMode(context, kCGBlendModeSourceOut);
//    CGContextSetFillColorWithColor(context, opaqueShadowColor);
//    CGContextAddPath(context, path);
//    CGContextFillPath(context);
//    CGContextEndTransparencyLayer(context);
//    
//    CGContextRestoreGState(context);
//    
//    CGColorRelease(opaqueShadowColor);
//}
+ (UIImage*) innerWhiteShadowWithImage3: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset  alpha: (float) alpha {
    CGRect imgRect = CGRectMake(0.0f, 0.0f, img.size.width, img.size.height);
    CGSize imgSz = imgRect.size;
    UIColor *clr = [UIColor colorWithWhite:1.0f alpha:alpha];
//    UIImage *invertedImage =
//    [self invertedImageWithMask:img color:[UIColor whiteColor]];
    
//    CGRect rect2 = CGRectMake(0, 0, imgSz.width+6, imgSz.height+6);
//    CGSizeMake(imgSz.width+6, imgSz.height+6)
    UIGraphicsBeginImageContextWithOptions(imgSz, FALSE, [UIScreen mainScreen].scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(ctx);
    
    CGContextAddEllipseInRect(ctx, imgRect);
    CGContextClip(ctx);
    
    CGColorRef opaqueShadowColor = CGColorCreateCopyWithAlpha(clr.CGColor, 1.0f);
    
    CGContextSetAlpha(ctx, CGColorGetAlpha(clr.CGColor));
    CGContextBeginTransparencyLayer(ctx, NULL);
    
    CGContextSetShadowWithColor(ctx, offset, blur, opaqueShadowColor);
    CGContextSetBlendMode(ctx, kCGBlendModeSourceOut);
    CGContextSetFillColorWithColor(ctx, opaqueShadowColor);
    CGContextAddEllipseInRect(ctx, imgRect);
    CGContextFillEllipseInRect(ctx, imgRect);
    
    CGContextEndTransparencyLayer(ctx);
    
    UIImage *shdwImg = UIGraphicsGetImageFromCurrentImageContext();
    CGContextRestoreGState(ctx);
    CGColorRelease(opaqueShadowColor);
    
    UIGraphicsEndImageContext();
    
    return shdwImg;
    
    
//    CGContextSaveGState(ctx); {
//        //Draw Background
//        CGContextSetRGBFillColor(ctx, 1.0f, 1.0f, 1.0f, 0.0f);
//        CGContextFillRect(ctx, imgRect);
//        
//        //        //Draw original Image
//        //        [img drawAtPoint:CGPointZero];
//        
//        CGContextClipToMask(ctx, imgRect, img.CGImage);
//        
//        //Setup shadow
//        UIColor *clr = [UIColor whiteColor];
//        
//        CGContextSetShadowWithColor(ctx, offset, blur, clr.CGColor);
//        //        [invertedImage drawInRect:CGRectMake((rect2.size.width-imgSz.width)/2, (rect2.size.height-imgSz.height)/2, rect2.size.width, rect2.size.height)];
//        [invertedImage drawInRect:imgRect];
//    }
//    UIImage *imgGlow = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    CGContextRestoreGState(ctx);
//    
//    return imgGlow;
//    uigraphicsbe
    
    
//    return [self drawInnerGlowWithMaskImage:img bounds:CGRectMake(3, 3, img.size.width+6, img.size.height+6) color:[UIColor whiteColor] offset:CGSizeZero blur:blurRadius];
    
//    UIGraphicsBeginImageContextWithOptions(imgSz, NO, [UIScreen mainScreen].scale); {
//        CGContextRef ctx = UIGraphicsGetCurrentContext();
//        
//        CGContextSaveGState(ctx);
//        
//        
//        
//        CGContextRestoreGState(ctx);
//        
//    }
//    UIGraphicsEndImageContext();
}

+ (UIImage*) innerWhiteShadowWithImage2: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset {
    return [self innerWhiteShadowWithImage2:img withBlur:blur offset:offset shadowColor:[UIColor whiteColor]];
}

+ (UIImage*) innerWhiteShadowWithImage2: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset shadowColor: (UIColor*) shadowColor {
    CGRect imgRect = CGRectMake(0.0f, 0.0f, img.size.width, img.size.height);
    CGSize imgSz = imgRect.size;
    UIImage *invertedImage =
    [self invertedImageWithMask:img color:[UIColor whiteColor]];
    
//    CGRect rect2 = CGRectMake(0, 0, imgSz.width+6, imgSz.height+6);
    
    UIGraphicsBeginImageContextWithOptions(imgSz, false, img.scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(ctx); {
//        CGContextTranslateCTM(ctx, 0, -imgSz.height);
//        CGContextScaleCTM(ctx, 1.0, -1.0);
        
        //Draw Background
        CGContextSetRGBFillColor(ctx, 0.0f, 0.0f, 0.0f, 0.0f);
        CGContextFillRect(ctx, imgRect);
        
//        //Draw original Image
        [img drawAtPoint:CGPointZero];
        
        CGContextTranslateCTM(ctx, 0, imgRect.size.height);
        CGContextScaleCTM(ctx, 1.0, -1.0);
        CGContextClipToMask(ctx, imgRect, img.CGImage);
        
        //Setup shadow
        UIColor *clr = shadowColor;
        
        CGContextSetShadowWithColor(ctx, offset, blur, clr.CGColor);
//        [invertedImage drawInRect:CGRectMake((rect2.size.width-imgSz.width)/2, (rect2.size.height-imgSz.height)/2, rect2.size.width, rect2.size.height)];
        [invertedImage drawInRect:imgRect];
    }
    UIImage *imgGlow = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
//    CGContextRestoreGState(ctx);
    
    return imgGlow;
}

+ (UIImage*) innerWhiteShadowWithImage: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset {
    CGRect imgRect = CGRectMake(0.0f, 0.0f, img.size.width, img.size.height);
    CGSize imgSz = imgRect.size;
    
    UIGraphicsBeginImageContextWithOptions(imgSz, false, [UIScreen mainScreen].scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //Draw black background
    CGContextSetRGBFillColor(ctx, 0.0f, 0.0f, 0.0f, 1.0f);
    CGContextFillRect(ctx, imgRect);
    
    //Adjust for different coordinate systems from UIKit and Core Graphics
    CGContextTranslateCTM(ctx, 0.0f, imgRect.size.height);
    CGContextScaleCTM(ctx, 1.0f, -1.0f);
    
    //Draw the original image
    CGContextDrawImage(ctx, imgRect, img.CGImage);
    
    CGContextSaveGState(ctx);
    
    //Clip the original image, so that we only draw the shadows on the inside of the image but nothing outside.
    CGContextClipToMask(ctx, imgRect, img.CGImage);
    
    //Set up the shadow
    UIColor *innerGlowColor = [UIColor whiteColor];
    CGContextSetShadowWithColor(ctx, offset, blur, innerGlowColor.CGColor);
    
    //Create a mask with an inverted alpha channel and draw it. This will cause the shadow to appear on the inside of our original image.
    CGImageRef mask = [self createMaskFromAlphaChannel:img];
    CGContextDrawImage(ctx, imgRect, mask);
    CGImageRelease(mask);
    
    UIImage * glowImage = UIGraphicsGetImageFromCurrentImageContext();
    
    CGContextRestoreGState(ctx);
    UIGraphicsEndImageContext();

    
    return glowImage;
}

+ (CGImageRef) createMaskFromAlphaChannel: (UIImage*) img {
    CGSize sz = img.size;
    size_t width = sz.width;
    size_t height = sz.height;
    
    NSMutableData *data = [NSMutableData dataWithLength:width * height];
//    UIGraphicsBeginImageContextWithOptions(sz, FALSE, [UIScreen mainScreen].scale);
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    
    CGContextRef ctx = CGBitmapContextCreate([data mutableBytes], width, height, 8, width * sizeof(uint32_t), CGColorSpaceCreateDeviceRGB(), kCGImageAlphaOnly);
//    CGContextRef ctx = CGBitmapContextCreate([data mutableBytes], width, height, 8, NULL, CGColorSpaceCreateDeviceRGB(), kCGImageAlphaOnly);

    
//    CGContextRef ctx = CGBitmapContextCreate([data mutableBytes], width, height, 8, NULL, width, kCGImageAlphaOnly);
    
    //Set the blend mode to copy to avoid any alteration of the source data
    CGContextSetBlendMode(ctx, kCGBlendModeCopy);
    
    //Draw the image to extract the alpha channel
    CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, width, height), img.CGImage);
    CGContextRelease(ctx);
    
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((__bridge CFMutableDataRef)data);
    
    CGImageRef maskingImage = CGImageMaskCreate(width, height, 8, 8, width, dataProvider, NULL, TRUE);
    
    return maskingImage;
}

//+ (UIImage*) createDefaultShadow: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset  alpha: (float) alpha {
//    CGRect imgRect = CGRectMake(0.0f, 0.0f, img.size.width, img.size.height);
//    CGSize imgSz = imgRect.size;
//    UIColor *clr = [UIColor colorWithWhite:1.0f alpha:alpha];
//    
//    UIGraphicsBeginImageContextWithOptions(imgSz, FALSE, [UIScreen mainScreen].scale);
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    
//    CGContextSaveGState(ctx);
//    
//    CGColorRef opaqueShadowColor = CGColorCreateCopyWithAlpha(clr.CGColor, 1.0f);
//    
//    CGContextSetAlpha(ctx, CGColorGetAlpha(clr.CGColor));
//    CGContextBeginTransparencyLayer(ctx, NULL);
//    
//    CGContextSetShadowWithColor(ctx, offset, blur, opaqueShadowColor);
//    CGContextSetBlendMode(ctx, kCGBlendModeSourceOut);
//    CGContextSetFillColorWithColor(ctx, opaqueShadowColor);
//    
//    CGContextEndTransparencyLayer(ctx);
//    
//    UIImage *shdwImg = UIGraphicsGetImageFromCurrentImageContext();
//    CGContextRestoreGState(ctx);
//    CGColorRelease(opaqueShadowColor);
//    
//    UIGraphicsEndImageContext();
//    
//    return shdwImg;
//}

@end
