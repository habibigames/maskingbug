//
//  GlowManager.h
//  VERTEX
//
//  Created by Paul Hammer on 23.03.16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlowManager : NSObject

+ (UIImage*)generateGlowImageByImage:(UIImage*) img hue: (CGFloat) hue saturation:(CGFloat)saturation brihgtness: (CGFloat) brightness alpha: (CGFloat) alpha offset:(CGSize)offset blur:(CGFloat)blur;
+ (UIImage*) createGlowImageByImage:(UIImage*) img alpha: (CGFloat) alpha offset:(CGSize)offset blur:(CGFloat)blur;
+ (UIImage*) createOuterGlowImageByImage:(UIImage *)img alpha:(CGFloat)alpha offset:(CGSize)offset blur:(CGFloat)blur;
+ (UIImage*) createOuterGlowImageByImage_withoutCuttingOutOriginalImage:(UIImage *)img alpha:(CGFloat)alpha offset:(CGSize)offset blur:(CGFloat)blur ;
//+ (UIImage*) createInnerGlowImageByImage:(UIImage *)img alpha:(CGFloat)alpha offset:(CGSize)offset blur:(CGFloat)blur;

//+ (UIImage*)drawInnerGlowWithMaskImage: (UIImage*) mask bounds:(CGRect)bounds color:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur;

+ (UIImage*) innerWhiteShadowWithImage: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset;
+ (UIImage*) innerWhiteShadowWithImage2: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset;
+ (UIImage*) innerWhiteShadowWithImage2: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset shadowColor: (UIColor*) shadowColor;
+ (UIImage*) innerWhiteShadowWithImage3: (UIImage*) img withBlur: (float) blur offset: (CGSize) offset  alpha: (float) alpha;

+ (UIImage *)imageWithShadowColor:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur byImage:(UIImage*) img;

+ (UIImage *)makeAHoleIn:(UIImage *)baseImage withAMask:(UIImage *)mask onPosition:(CGPoint)position;
+ (UIImage *) mergeImage:(UIImage *)backImage with:(UIImage *)frontImage onPosition:(CGPoint)position;
@end
