//
//  CEAnnotationItem.h
//  MaskingBug
//
//  Created by Paul Hammer on 17.07.16.
//  Copyright © 2016 Paul Willy Hammer. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    
    //Fade-In   - Flüssig
    CEAT_1_1_1,
    CEAT_1_1_2,
    CEAT_1_1_3,
    
    // - //     - Stockend
    CEAT_1_2_1,
    
    //Spin-In
    CEAT_2_1_1,
    CEAT_2_1_2,
    
    // - //     - Stockend
    
    CEAT_2_2_1,
    
} CEAnnotationItem_AnimationType;

@interface CEAnnotationItem : NSObject {
    CEAnnotationItem_AnimationType animType;
    NSString    *text;
    CCColor     *clr;
    
    CCSprite  *s;
}

/**
 *  Returns a new created Annotation. Second at Annotation Sprite to Annotation View. Thirdly perfom start annimation of Annotation Item.
 *
 *  @param _txt Text of the Annotation
 *  @param _clr Color
 *  @param _aT  Anomation Type
 *
 *  @return instance of annotation Item.
 */
- (instancetype) initTextItem: (NSString*) _txt color: (CCColor*) _clr animation: (CEAnnotationItem_AnimationType) _aT;

/**
 *  TAKE CARE for adding  annotation sprite to scene first.
 */
- (void) startAnnotation;

- (CCSprite*) getSprite;

- (CCAction*) getAnimation;

@end
