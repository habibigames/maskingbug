#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D u_MaskTexture;
uniform vec2 u_MaskTextureSize; // content size in points
uniform vec2 u_MaskTexturePixelSize; // content size in pixels

void main()
{
    vec4 normalColor = texture2D(cc_MainTexture, cc_FragTexCoord1);
    vec4 maskColor = texture2D(u_MaskTexture, cc_FragTexCoord1);
    gl_FragColor = vec4(maskColor.r, maskColor.g, maskColor.b, normalColor.a);
}