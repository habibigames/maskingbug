//
//  CE_Label.h
//  VERTEX
//
//  Created by Paul Hammer on 24.05.16.
//  Copyright © 2016 ColossalEntertainment. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"

@interface CE_Label : CCLayoutBox {
    
}

+ (instancetype) labelWithFontName: (NSString*) fontName fontSize: (float) fontSize text: (NSString*) text andGlyphName: (NSString*) glyphName andContentSize: (CGSize) cntSize;
- (CCLabelTTF*) textLabel;
- (void) updateImage_withImageNamed: (NSString*) imgName;

@end
