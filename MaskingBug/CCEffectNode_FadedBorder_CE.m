//
//  CCEffectNode_FadedBorder_CE.m
//  MaskingBug
//
//  Created by Paul Hammer on 23.08.16.
//  Copyright © 2016 Paul Willy Hammer. All rights reserved.
//

#import "CCEffectNode_FadedBorder_CE.h"
#import "CCTextureCache.h"

static CCTexture *_texBottom, *_texTop, *_texBottomNTop;

@implementation CCEffectNode_FadedBorder_CE

+ (instancetype) containerWithWidth:(float)w height:(float)h {
    CCEffectNode_FadedBorder_CE* maskedCnt = [CCEffectNode_FadedBorder_CE effectNodeWithWidth:(int)w height:(int)h pixelFormat:CCTexturePixelFormat_RGBA8888];
    CCTexture* __mask = [CCEffectNode_FadedBorder_CE createTransparencyTextureMaskWithSizeee:CGSizeMake((int)w, (int)h) fadeEffectOnBottom:TRUE onTop:TRUE];
    [maskedCnt setMask:__mask];
    [maskedCnt setup];
    return maskedCnt;
}

- (void) setMask: (CCTexture*) _m {
    bool setupAgain = false;
    if (mask) {
        mask = nil;
        setupAgain = true;
    }
    mask = _m;
    
    if (setupAgain)
        [self setup];
    
}

- (void) setup {
    if (mask) {
        [[self sprite] shaderUniforms][@"u_MaskTexture"]    = mask;
        [[self sprite] setShader:   [CCShader shaderNamed:@"MaskPositive"]];
        [self setClearColor:        [CCColor clearColor]];
        [self setClearFlags:        (GLbitfield) GL_COLOR_BUFFER_BIT];
        [self setAutoDraw:          TRUE];
    }
}

- (CGPoint) getCenterForSize: (CGSize) sz {
    return ccp( ( sz.width > 0  ? sz.width  /2.0 : 0 ) ,
               ( sz.height > 0 ? sz.height /2.0 : 0 ));
}

- (void) switchMaskBottom: (BOOL) onBottom NTop: (BOOL) onTop {
    //    NSString *_texKey = @"";
    CCTexture *neededTexture;
    if (onBottom && onTop) {
        //        _texKey = @"transparencyMaskBottonNTop";
        if (!_texBottomNTop)
            _texBottomNTop  = [CCEffectNode_FadedBorder_CE createTransparencyTextureMaskWithSizeee:self.contentSizeInPoints fadeEffectOnBottom:TRUE onTop:TRUE];
        neededTexture = _texBottomNTop;
    }
    else if (onBottom && !onTop) {
        //        _texKey = @"transparencyMaskBotton";
        if (!_texBottom)
            _texBottom      = [CCEffectNode_FadedBorder_CE createTransparencyTextureMaskWithSizeee:self.contentSizeInPoints fadeEffectOnBottom:TRUE onTop:FALSE];
        neededTexture = _texBottom;
    }
    else if (!onBottom && onTop ) {
        //        _texKey = @"transparencyMaskTop";
        if (!_texTop)
            _texTop         = [CCEffectNode_FadedBorder_CE createTransparencyTextureMaskWithSizeee:self.contentSizeInPoints fadeEffectOnBottom:FALSE onTop:TRUE];
        neededTexture = _texTop;
    }
    [self setMask:neededTexture];
}

#pragma mask Transparence Mask Handling
+ (CCTexture*) createTransparencyTextureMaskWithSizeee: (CGSize) sz fadeEffectOnBottom: (BOOL) onBottom onTop: (BOOL) onTop {
    const float gradientBorderHeight    = transparencyTextureMask_GradientBorderHeight;
    float width     = sz.width;
    float height    = sz.height;
    
    //Colors
    CCColor *fadingFromClr  = [CCColor whiteColor];
    CCColor *fadingToClr    = [CCColor colorWithWhite:1 alpha:0];
    
    
    //Container Node for colored Nodes
    CCSprite *scissorRect   = [[CCSprite alloc] init];
    [scissorRect            setContentSizeInPoints:CGSizeMake(width, height)];
    
    NSString *_texKey = @"";
    if (onTop && onBottom) { //Both
        _texKey = @"transparencyMaskBottonNTop";
        //Color node in the middle of the following two nodes
        CCNodeColor *clrNode = [CCNodeColor nodeWithColor:fadingFromClr width:width height:height-gradientBorderHeight*2.0f];
        
        [clrNode        setContentSizeInPoints:CGSizeMake(width, height - gradientBorderHeight*2.0f)];
        
        [clrNode        setAnchorPoint:ccp(0.5, 0.5)];
        
        [scissorRect    addChild:clrNode z:1];
        
        [clrNode        setPositionType:CCPositionTypeNormalized];
        
        [clrNode        setPosition:ccp(0.5f, 0.5f)];
        
        //Gradient node on top of container node
        CCNodeGradient *gradient_top
        = [CCNodeGradient nodeWithColor: fadingFromClr fadingTo: fadingToClr alongVector:ccp(0, -1)];
        
        [gradient_top   setContentSizeInPoints:CGSizeMake(width, gradientBorderHeight)];
        
        [scissorRect    addChild:gradient_top z:1];
        
        [gradient_top   setAnchorPoint:ccp(0.5, 1)];
        
        [gradient_top   setPositionType:CCPositionTypeNormalized];
        
        [gradient_top   setPosition:ccp(0.5f, 1.0f)];
        
        
        
        //Gradient node on bottom of container node
        CCNodeGradient *gradient_bottom
        = [CCNodeGradient nodeWithColor: fadingFromClr fadingTo: fadingToClr alongVector:ccp(0, 1)];
        
        [gradient_bottom setContentSizeInPoints:CGSizeMake(width, gradientBorderHeight)];
        
        [scissorRect addChild:gradient_bottom z:1];
        
        [gradient_bottom setAnchorPoint:ccp(0.5, 0)];
        
        [gradient_bottom setPositionType:CCPositionTypeNormalized];
        
        [gradient_bottom setPosition:ccp(0.5f, 0.0f)];
        
    }
    else if (!onTop && onBottom) { //Bottom
        _texKey = @"transparencyMaskBotton";
        //Color node in the middle of the following two nodes
        CCNodeColor *clrNode = [CCNodeColor nodeWithColor:fadingFromClr width:width height:height-gradientBorderHeight*2.0f];
        
        [clrNode        setContentSizeInPoints:CGSizeMake(width, height - gradientBorderHeight)];
        
        [clrNode        setAnchorPoint:ccp(0.5, 1.0)];
        
        [scissorRect    addChild:clrNode z:1];
        
        [clrNode        setPositionType:CCPositionTypeNormalized];
        
        [clrNode        setPosition:ccp(0.5f, 1.0f)];
        
        
        //Gradient node on bottom of container node
        CCNodeGradient *gradient_bottom
        = [CCNodeGradient nodeWithColor: fadingFromClr fadingTo: fadingToClr alongVector:ccp(0, 1)];
        
        [gradient_bottom setContentSizeInPoints:CGSizeMake(width, gradientBorderHeight)];
        
        [scissorRect addChild:gradient_bottom z:1];
        
        [gradient_bottom setAnchorPoint:ccp(0.5, 0)];
        
        [gradient_bottom setPositionType:CCPositionTypeNormalized];
        
        [gradient_bottom setPosition:ccp(0.5f, 0.0f)];
        
    }
    else if (onTop && !onBottom) { //Top
        _texKey = @"transparencyMaskTop";
        //Color node in the middle of the following two nodes
        CCNodeColor *clrNode = [CCNodeColor nodeWithColor:fadingFromClr width:width height:height-gradientBorderHeight*2.0f];
        
        [clrNode        setContentSizeInPoints:CGSizeMake(width, height - gradientBorderHeight)];
        
        [clrNode        setAnchorPoint:ccp(0.5, 0.0)];
        
        [scissorRect    addChild:clrNode z:1];
        
        [clrNode        setPositionType:CCPositionTypeNormalized];
        
        [clrNode        setPosition:ccp(0.5f, 0.0)];
        
        //Gradient node on top of container node
        CCNodeGradient *gradient_top
        = [CCNodeGradient nodeWithColor: fadingFromClr fadingTo: fadingToClr alongVector:ccp(0, -1)];
        
        [gradient_top   setContentSizeInPoints:CGSizeMake(width, gradientBorderHeight)];
        
        [scissorRect    addChild:gradient_top z:1];
        
        [gradient_top   setAnchorPoint:ccp(0.5, 1)];
        
        [gradient_top   setPositionType:CCPositionTypeNormalized];
        
        [gradient_top   setPosition:ccp(0.5f, 1.0f)];
        
    }
    
    
    //Set AnchorPoint and Position of Container node
    [scissorRect    setAnchorPoint: ccp(0.0, 0.0)];
    [scissorRect    setPosition:    ccp(0, 0)];
    
    CCRenderTexture *rend   = [CCRenderTexture renderTextureWithWidth:width height:height];
    [rend beginWithClear:   0 g:0 b:0 a:0];
    //    [scissorRect setFlipY:true];
    [scissorRect            visit];
    //    [scissorRect setFlipY:false];
    [rend end];
    
    //comment out the following for displaying the mask
    //    CGRect rect     = self.boundingBox;
    //    [self addChild: scissorRect];
    //    [scissorRect    setPosition:ccp(rect.size.width/2, rect.size.height/2)];
    
    //    return rend.texture;
    CCTexture *finalTexture = [[CCTextureCache sharedTextureCache] addCGImage:rend.getUIImage.CGImage
                                                                       forKey:_texKey];
    
    if      (onBottom && onTop)
        _texBottomNTop  = finalTexture;
    else if (onBottom && !onTop)
        _texBottom      = finalTexture;
    else if (!onBottom && onTop)
        _texTop         = finalTexture;
    
    return finalTexture;
}

@end
