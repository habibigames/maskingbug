//
//  GameConfiguration.h
//  VERTEX
//
//  Created by Paul Hammer on 09.02.16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#ifndef GameConfiguration_h
#define GameConfiguration_h

#define color_0 @"#00ffff"
#define color_1 @"#00ff00"
#define color_2 @"#ffff00"
#define color_3 @"#ff7800"
#define color_4 @"#ff00a0"
#define color_highlighted @"#101010"

#define kWebAdress @"http://pwhsoft.de"
#define AppID 1099790830

//////////////////////////////////////////////////////////////////////////////////////////////////
//enums
//////////////////////////////////////////////////////////////////////////////////////////////////
typedef enum : NSUInteger {
    colorType_Cyan,
    colorType_Green,
    colorType_Yellow,
    colorType_Orange,
    colorType_Pink,
    colorType_White,
} ColorType_CE;

enum RotationDirections {
    rotation_none,
    rotation_left,
    rotation_right,
    rotation_left_fast,
    rotation_right_fast,
};

enum MenuButtonTypes {
    menuButtonType_none,
    menuButtonType_settings,
    menuButtonType_ranking,
    menuButtonType_statistics,
    menuButtonType_empty,
};

enum Z_GameOverView_Edges {
    Z_GAMEOVER_Background,
    Z_GAMEOVER_Labels,
    Z_GAMEOVER_Buttons,
};

typedef enum : NSUInteger {
    Options_ItemInfo_attribute_enableGlow,
    Options_ItemInfo_attribute_customFontSize,
    Options_ItemInfo_attribute_enableMultiline,
} Options_ItemInfo_attribute;
typedef NSDictionary<NSString*, id> Options_ItemInfo_attributes;

typedef enum : NSUInteger {
    SaveGame_sound_onoff,
    SaveGame_music_onoff,
    SaveGame_isPurchased,
    SaveGame_roundsPlayed,
    SaveGame_highscore,
    SaveGame_pointsTotal,
    SaveGame_longestRound,
    SaveGame_TimeTotal,
    SaveGame_blueBlocks,
    SaveGame_greenBlocks,
    SaveGame_yellowBlocks,
    SaveGame_orangeBlocks,
    SaveGame_pinkBlocks,
    SaveGame_CntAchievements,
    SaveGame_SliderSensitivity,
    SaveGame_Annotations_onoff,
    SaveGame_friendsScore_OfflineDictionary,
    SaveGame_ads_banner_mainscreen,
    SaveGame_ads_fullScreenBanner_mainscreen,
    SaveGame_ads_Video_mainscreen,
    SaveGame_custom_achievementBanner_enabled,
    SaveGame_webadress,
    SaveGame_AchievedAchievementDescriptionsDictionary,
} SaveGame_Key;

//////////////////////////////////////////////////////////////////////////////////////////////////
//Game Constants
//////////////////////////////////////////////////////////////////////////////////////////////////
#define default_fontName                            @"RussellSquare"

#define rotationDirectionAmount                     5

#define defaultCircleSize                           5000
#define brickRingsAmount                            4
#define middleCircleSpriteRadiusExtension           2.0f
#define defaultRingRadiusIncreaseFactor             0.2/4 //Ring old rd + factor = new rd -> increased by this factor on each frame
//#define defaultRingRadiusIncreaseFactor           0.075/4 //Ring old rd + factor = new rd -> increased by this factor on each frame
//#define defaultRingRadiusIncreaseFactor           0.075 //Ring old rd + factor = new rd -> increased by this factor on each frame

#define Orbit_Line_Thickness                        1.0f

//Player Pad
//mark Player Ball Angle Handling#define playerPad_Ball_MinAngle_Middle 67.5f
#define playerPad_Ball_MinAngle_Middle              50.5f
#define playerPad_Ball_MinAngle_NextToMiddle        67.5f
#define playerPad_Ball_MinAngle_Outer               80.5f
//#define playerPad_Ball_MinAngle_Middle            80.5f
//#define playerPad_Ball_MinAngle_NextToMiddle      67.5f
//#define playerPad_Ball_MinAngle_Outer             40.5f
#define playerPad_Glow_Opacity                      0.5f
#define playerPad_Glow_Radius                       45.f
#define playerPad_Glow_Enabled                      false

//Player Ball
#define defaultBallSpeed                            78.0f
#define possibleSpeedDifference                     10.0f //Min Speed = Ball Speed - possibleSpeedDifference
//#define defaultBallSpeed 25
#define playerBall_GlowRadius                       15.0f
#define playerBall_GlowOpacity                      0.7f
#define defaultPlayerBallSize                       10.0f

///////////////////////////////////////

#define ringDistance                                1.5f
#define ringSizeInPercent                           0.25f

//mark Colorize
#define tintToDuration_PadNPadControl               0.2f //-> Colorizing Speed
#define tintToDuration_PlayerBall                   0.2f //-> Colorizing Speed
#define tintToDuration_ScoreHUD                     0.2f //-> Colorizing Speed
#define tintToDuration_BackgroundStripes            0.5f //-> Colorizing Speed
//#define background_zoomIncreaseFactor     1.2f //scales the

//Special Blocks - Durations in seconds
//Orange - Invert Control
#define specialBlock_Orange_Duration                    6.0f
#define specialBlock_Orange_WaitBeforeStart_Duration    0.5f

//Green - Small Paddle
#define specialBlock_Green_Duration                 6.0f

//Yellow - Highspeed
#define specialBlock_Yellow_Duration                6.0f
#define specialBlock_Yellow_SpeedIncreaseFactor     1.5f

//Pink - Multiball
#define specialBlock_Pink_BallAmount                3
#define specialBlock_Pink_minAngle                  10.0f
#define specialBlock_Pink_maxAngle                  24.0f
#define specialBlock_Pink_BallSpeedDifference_From  5.0f //From Speed Difference
#define specialBlock_Pink_BallSpeedDifference_To    10.0f //To Speed Difference

//Menu
#define menu_tintToDuration_BackgroundStripes       1.5f //-> Colorizing Speed
#define menu_tintTo_WaitDelay_BackgroundStripes     1.5f //-> Wait Delay before changing color
#define menu_button_increaseFactor                  1.2f

//////////////////////////////////////////////////////////////////////////////////////////////////
//Image Names
//////////////////////////////////////////////////////////////////////////////////////////////////
#define options_white                       @"w_menuIcon_options.png"
#define options_black                       @"b_menuIcon_options.png"
#define ranking_white                       @"w_menuIcon_ranking.png"
#define ranking_black                       @"b_menuIcon_ranking.png"
#define statistics_white                    @"w_menuIcon_statistics.png"
#define statistics_black                    @"b_menuIcon_statistics.png"
#define play_white                          @"w_menuIcon_play.png"
#define play_black                          @"b_menuIcon_play.png"
#define back_white                          @"w_icon_back.png"
#define compete_white                       @"w_icon_compete.png"
#define replay_white                        @"w_icon_replay.png"
#define share_white                         @"w_menuIcon_share.png"
#define switchButton_On                     @"toggleButton_on.png"
#define switchButton_Off                    @"toggleButton_off.png"
#define slidersensitivity                   @"slidersensitivity.png"
#define slidersensitivity_ball              @"slidersensitivity_ball.png"

//////////////////////////////////////////////////////////////////////////////////////////////////
//Statistics
//Blocks
//////////////////////////////////////////////////////////////////////////////////////////////////
#define statistics_icon_block_blue          @"statistics_blocksBlue.png"
#define statistics_icon_block_green         @"statistics_blocksGreen.png"
#define statistics_icon_block_orange        @"statistics_blocksOrange.png"
#define statistics_icon_block_pink          @"statistics_blocksPink.png"
#define statistics_icon_block_yellow        @"statistics_blocksYellow.png"
//Time and Trophy
#define statistics_icon_time                @"statistics_time.png"
#define statistics_icon_trophy              @"statistics_trophy.png"
#define statistics_icon_play                @"statistics_play.png"


//////////////////////////////////////////////////////////////////////////////////////////////////
//Blur Effect
//////////////////////////////////////////////////////////////////////////////////////////////////
#define _blurRadius     30.f
#define glowAlpha       0.8f

//////////////////////////////////////////////////////////////////////////////////////////////////
//Menu and GameOver Glow Effect
//////////////////////////////////////////////////////////////////////////////////////////////////
#define menu_Radius_InnerGlow                               40.0f
#define menu_Radius_OuterGlow                               20.0f
#define menu_Opacity_InnerGlow                              0.55f
#define menu_Opacity_OuterGlow                              0.7f
#define menu_Button_InnerCircleRadiusDifference             1.0f
#define menu_Label_ScoredAndBest_FontSize                   16.f
#define menu_Label_CatchPhrase_FontSize                     12.0f
#define menu_Opacity_Stripes                                0.45f
#define menu_GlowRadius_Title                               20.0f
#define menu_button_width_small                             32.0f
#define menu_button_width_large                             55.0f
#define menu_button_YPercentageLocationReferencedToParentSizeHeigth 0.1f
#define menu_button_Radius_InnerGlow                        20.0f
#define menu_button_Radius_OuterGlow                        30.0f
#define menu_button_Opacity_InnerGlow                       0.55f
#define menu_button_Opacity_OuterGlow                       0.55f
#define menu_button_Radius_MainImage                        10.0f
#define menu_button_Opacity_MainImage                       0.65f
#define menu_SwitchButton_Label_FontSize                    10.0f

//////////////////////////////////////////////////////////////////////////////////////////////////
//Menu Tables
//////////////////////////////////////////////////////////////////////////////////////////////////
#define table_minRowHeight                                  35.0f
#define table_minRowHeight_line                             20.0f
#define table_minRowHeight_line_2                           10.0f
#define table_minRowHeight_statistics                       20.0f
#define table_minRowHeight_button                           20.0f
#define table_minRowHeight_credits                          15.0f
#define table_creditsHeadlineItem_fontsize                  14.0f
#define table_credits_copyright_fontsize                    7.0f
#define table_fontSize_Labels                               10.0f
#define tabel_credits_spaceLineHeight                       7.0f
#define tabel_credits_spaceLineHeight_beforeCredits         14.0f
#define table_MaxWidthScalingFactorReflectedToBoundingBox   0.7f
#define transparencyTextureMask_GradientBorderHeight        45.0f

//////////////////////////////////////////////////////////////////////////////////////////////////
//Menu animation
//////////////////////////////////////////////////////////////////////////////////////////////////
#define submenu_changeStripesOpacityForMenuChange_Duration  0.5f //should be the same value as opening and closing animation duration (as example: submenu_openingAnimation_Duration)
#define submenu_changeStripesOpacityForMenuChange_Delay     0.0f

#define submenu_waitDelay_beforeStartOpeningAnimation       0.35f
#define submenu_waitDelay_beforeStartClosingAnimation       0.35f

#define submenu_openingAnimation_Duration                   0.28f
#define submenu_closingAnimation_Duration                   0.28f

#define submenu_openingAnimation_Menu_OpacityChange_Duration                0.25f
#define submenu_openingAnimation_MiddleCircleSprite_OpacityChange_Duration  0.3f


//////////////////////////////////////////////////////////////////////////////////////////////////
//Preloading
//////////////////////////////////////////////////////////////////////////////////////////////////
#define preloadingAnimation_CircleWidth                             25.46f//35.46f //old value = 40.46f
//#define preloadingAnimation_Min_Duration 5.8f //Duration in seconds
#define preloadingAnimation_Min_Duration                            1.8f //Duration in seconds
#define afterPreloading_ScalingAction_ScaleTo_Value_PadControl      1.0f
#define afterPreloading_ScalingAction_ScaleTo_Duration_PadControl   0.6f
#define afterPreloading_ScalingAction_ScaleTo_Duration_MiddleCircle 0.6f
#define afterPreloading_ScalingAction_ScaleTo_Duration_MainMenu     0.6f
#define afterPreloading_ScalingAction_ScaleTo_Duration_BrickRings   0.5f
#define afterPreloading_ScalingAction_ScaleTo_Duration_Logo         1.0f
#define afterPreloading_ScalingAction_MoveTo_Duration_Logo          1.0f
#define afterPreloading_WaitDelay_BeforeScaling_PadControlAndMiddleCircleSprite 0.7f //starts x seconds after Logo animation has started
#define afterPreloading_WaitDelay_BeforeScaling_BrickRings                      0.2f //starts x seconds after PadControl and MiddleCircle Sprite ScalingAction started

//////////////////////////////////////////////////////////////////////////////////////////////////
// GAME CENTER
//////////////////////////////////////////////////////////////////////////////////////////////////
//Leaderboards
//////////////
#define gc_ranksScore                   @"de.colossalentertainment.vertex.gc.ranks.score"
#define gc_ranksTime                    @"de.colossalentertainment.vertex.gc.ranks.time"

//////////////////////////////////////////////////////////////////////////////////////////////////
//Achievements
//////////////////////////////////////////////////////////////////////////////////////////////////
//Score
#define gc_achievement_score50          @"de.colossalentertainment.vertex.gc.achievements.score50"
#define gc_achievement_score100         @"de.colossalentertainment.vertex.gc.achievements.score100"
#define gc_achievement_score200         @"de.colossalentertainment.vertex.gc.achievements.score200"
#define gc_achievement_score500         @"de.colossalentertainment.vertex.gc.achievements.score500"
#define gc_achievement_score1000        @"de.colossalentertainment.vertex.gc.achievements.score1000"

//Times
#define gc_achievement_time60           @"de.colossalentertainment.vertex.gc.achievements.time60"
#define gc_achievement_time120          @"de.colossalentertainment.vertex.gc.achievements.time120"
#define gc_achievement_time300          @"de.colossalentertainment.vertex.gc.achievements.time300"
#define gc_achievement_time600          @"de.colossalentertainment.vertex.gc.achievements.time600"

//Ranks
#define gc_achievement_rank1            @"de.colossalentertainment.vertex.gc.achievements.rank1"
#define gc_achievement_rank2            @"de.colossalentertainment.vertex.gc.achievements.rank2"
#define gc_achievement_rank3            @"de.colossalentertainment.vertex.gc.achievements.rank3"
#define gc_achievement_rank4            @"de.colossalentertainment.vertex.gc.achievements.rank4"
#define gc_achievement_rank5            @"de.colossalentertainment.vertex.gc.achievements.rank5"
#define gc_achievement_rank6            @"de.colossalentertainment.vertex.gc.achievements.rank6"
#define gc_achievement_rank7            @"de.colossalentertainment.vertex.gc.achievements.rank7"
#define gc_achievement_rank8            @"de.colossalentertainment.vertex.gc.achievements.rank8"
#define gc_achievement_rank9            @"de.colossalentertainment.vertex.gc.achievements.rank9"
#define gc_achievement_rank10           @"de.colossalentertainment.vertex.gc.achievements.rank10"

//Blocks
#define gc_achievement_pink100          @"de.colossalentertainment.vertex.gc.achievements.pink100"
#define gc_achievement_orange100        @"de.colossalentertainment.vertex.gc.achievements.orange100"
#define gc_achievement_green100         @"de.colossalentertainment.vertex.gc.achievements.green100"
#define gc_achievement_yellow100        @"de.colossalentertainment.vertex.gc.achievements.yellow100"
#define gc_achievement_pink1000         @"de.colossalentertainment.vertex.gc.achievements.pink1000"
#define gc_achievement_orange1000       @"de.colossalentertainment.vertex.gc.achievements.orange1000"
#define gc_achievement_green1000        @"de.colossalentertainment.vertex.gc.achievements.green1000"
#define gc_achievement_yellow1000       @"de.colossalentertainment.vertex.gc.achievements.yellow1000"

#define gc_achievementsTotal            27

//////////////////////////////////////////////////////////////////////////////////////////////////
// Google
//////////////////////////////////////////////////////////////////////////////////////////////////
#define admob_BannerAdUnitID_ios            @"ca-app-pub-4185516150506232/6280176900"
#define admob_BannerAdUnitID_android        @"ca-app-pub-3940256099942544/2934735716"
#define admob_InterstitialAdUnitID_ios      @"ca-app-pub-4185516150506232/6720828909"
#define admob_InterstitialAdUnitID_android  @"ca-app-pub-3940256099942544/2934735716"

#define admob_Video_ios                     @"ca-app-pub-4185516150506232/7859484902"
#define admob_Video_android                 @"ca-app-pub-4185516150506232/7859484902"

#define admob_testDeviceID                  @"178265d02caed4b300537d299ac48b0b"

//////////////////////////////////////////////////////////////////////////////////////////////////
//Remote Config Keys
//////////////////////////////////////////////////////////////////////////////////////////////////
//Google - Remote Control
#define developerMode_activated                 true
//////////////////////////////////////////////////////////////////////////////////////////////////
//Keys
#define rcK_ads_banner_mainscreen               @"ads_banner_mainscreen"
#define rcK_ads_fullScreenBanner_mainscreen     @"ads_fullScreenBanner_mainscreen"
#define rcK_ads_video_mainscreen                @"ads_video_mainscreen"
#define rcK_webadress                           @"webadress"
#define rcK_custom_achievementBanner_enabled    @"custom_achievementBanner_enabled"

//////////////////////////////////////////////////////////////////////////////////////////////////
// Custom Achievement Banner
//////////////////////////////////////////////////////////////////////////////////////////////////
#define achievementNotification_BannerBackground    @"achievementBannerBackground.jpg"
#define achievementNotification_IconGlow            @"achievementBannerIconGlow.png"
#define achievementNotification_IconMask            @"achievementBannerIconMask.png"
#define achievementNotification_StripeGlow          @"achievementBannerStripeGlow.png"

//Texts
#define achievementNotification_Title                   @"ACHIEVEMENT UNLOCKED"
#define achievementNotification_Title_underlined        true
#define achievementNotification_FontName                @"RussellSquare"
#define achievementNotification_Title_FontSize          14.0
#define achievementNotification_Description_FontSize    10.0
#define achievementNotification_MinimumFontSize         5.0

#define achievementBannerTestEnabled                    false

#define achievementBanner_Animation_duration_main       5.0 //Waits the given delay as given
#define achievementBanner_Animation_duration_Openclose  0.4 //Opening/Closing Speed of the Achievement Notification Banner

/**
 *  In-App Store
 */
#define inapp_purchase_disable_ads_id_ios           @"de.colossalentertainment.vertex.purchases.adfree"
#define inapp_purchase_disable_ads_id_android       @""
#define removeAds_Message                           @"Within a simple purchase you can remove all the Ads and you are supporting us.\nWithout these ads you can get more Focus!"
#define removeAds_Message_Title                     @"REMOVE ADS"
#define removeAds_Message_CancelButtonText          @"Maybe Later"
#define removeAds_Message_RestoreButtonText         @"or Restore Purchase"
#define removeAds_Message_PurchaseButtonText        @"YES, REMOVE!!!"

/**
 *  Spent Cup of Coffee - In-App purchase
 */
#define inapp_purchaseSpent_ios_spentCoffe          @"de.colossalentertainment.vertex.purchases.spentCoffee"
#define inapp_purchaseSpent_ios_spentCoffeNBagels   @"de.colossalentertainment.vertex.purchases.spentCoffeeAndBagels"
#define inapp_purchaseSpent_android                 @""
#define inapp_purchaseSpent_Message                 @"Spent a ..."
#define inapp_purchaseSpent_Message_Title           @"Spent a cup of coffee"
#define inapp_purchaseSpent_Message_CancelButtonText @"Maybe Later"
#define inapp_purchaseSpent_Message_ButtonText_Coffee        @"Cup of Coffee"
#define inapp_purchaseSpent_Message_ButtonText_CoffeeNBagels @"Cup of Coffee and some Bagels too"

//TODO: MessageBox --> "Thank you for your donation"

/**
 *  Product List
 */
#define inappProductList [NSSet setWithObjects: inapp_purchase_disable_ads_id_ios, inapp_purchaseSpent_ios_spentCoffe, inapp_purchaseSpent_ios_spentCoffeNBagels, nil];

/**
 *  Annotations
 */

#define annotationTestsEnabled                      true


#define submenu_table_heightInPercent                       0.63
#define submenu_table_posY_inPercent                        0.48

#endif /* GameConfiguration_h */
