//
//  NSString+Extended.m
//  VERTEX
//
//  Created by Paul Hammer on 14.01.16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "NSString+Extended.h"

@implementation NSString (Extended)
+ (NSString*) collisionIdentifier:(int)value {
    return [NSString stringWithFormat:@"_%i", value];
}
+ (NSString*) stringForUInt: (NSUInteger) val {
    return [NSString stringWithFormat:@"%lu", (unsigned long)val];
}
+ (NSString*) stringForBOOL: (BOOL) val {
    return [NSString stringWithFormat:@"%lu", (unsigned long)val];
}
+ (NSString*) stringForInteger: (int) val {
    return [NSString stringWithFormat:@"%i", val];
}
+ (NSString*) stringForFloat: (float) val {
    return [NSString stringWithFormat:@"%f", val];
}
+ (NSString*) stringForFloat: (float) val decimalPlaces: (NSUInteger) decimalPlaces {
    NSMutableString *decimalManipulationStr = [NSMutableString stringWithString:[NSString stringForFloat:val]];
    
    if (decimalPlaces > 0 && [decimalManipulationStr containsString:@"."]) {
        NSArray<NSString*> *numberComponents = [decimalManipulationStr componentsSeparatedByString:@"."];
        decimalManipulationStr = nil;
        decimalManipulationStr = [NSMutableString stringWithString:[numberComponents objectAtIndex:0]];
        [decimalManipulationStr appendString:@"."];
        [decimalManipulationStr appendString:[[numberComponents lastObject] substringWithRange:NSMakeRange(0, decimalPlaces)]];
    }
    
    return [NSString stringWithString:decimalManipulationStr];
}

+ (NSString*) dateTimeStringForLong: (NSTimeInterval) interval {
    NSNumber *numInterval = [NSNumber numberWithDouble:interval];
    
    int inputSeconds = [numInterval intValue];
    int hours =  inputSeconds / 3600;
    int minutes = ( inputSeconds - hours * 3600 ) / 60;
    int seconds = inputSeconds - hours * 3600 - minutes * 60;
    
    NSMutableString *timeStr = [NSMutableString stringWithString:@""];
    
    if (hours > 0)
        [timeStr appendFormat:@"%.2d:", hours];
    
    [timeStr appendFormat:@"%.2d:%.2d", minutes, seconds];
    
    return [NSString stringWithString:timeStr];
}

@end
