//
//  NSString+Extended.h
//  VERTEX
//
//  Created by Paul Hammer on 14.01.16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extended)

+ (NSString*) collisionIdentifier:(int)value;
+ (NSString*) stringForUInt: (NSUInteger) val;
+ (NSString*) stringForBOOL: (BOOL) val;
+ (NSString*) stringForInteger: (int) val;
+ (NSString*) stringForFloat: (float) val;
+ (NSString*) stringForFloat: (float) val decimalPlaces: (NSUInteger) decimalPlaces;
+ (NSString*) dateTimeStringForLong: (NSTimeInterval) interval;

@end
