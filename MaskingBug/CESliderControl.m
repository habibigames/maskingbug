//
//  CESliderControl.m
//  MaskingBug
//
//  Created by Paul Hammer on 31.07.16.
//  Copyright © 2016 Paul Willy Hammer. All rights reserved.
//

#import "CESliderControl.h"
#import "CCTableView.h"
#import "CCControlSubclass.h"
#import "CCTouch.h"

@implementation CESliderControl

//- (instancetype) init {
//    if (!self)
//        self = [super init];
//    return self;

- (CESliderControl*) initWithBackground:(CCSpriteFrame *)background andHandleImage:(CCSpriteFrame *)handle {
    return [super initWithBackground:background andHandleImage:handle];
}

- (void) touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event {
    [self touch_started];
    [super touchBegan:touch withEvent:event];
}

- (void) touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event {
    [self touch_ended];
    [super touchEnded:touch withEvent:event];
}

- (void) touchCancelled:(CCTouch *)touch withEvent:(CCTouchEvent *)event {
    [self touch_ended];
    [super touchCancelled:touch withEvent:event];
}

- (void) touch_started {
    if (![self doesParentExists])           return;
    if (![self parentIsTypeOf_tableView])   return;
    
    [(CCTableView*) self.parent setUserInteractionEnabled:  false];
}

- (void) touch_ended {
    if (![self doesParentExists])           return;
    if (![self parentIsTypeOf_tableView])   return;
    
    [(CCTableView*) self.parent setUserInteractionEnabled:  true];
}

- (BOOL) doesParentExists {
    return (self.parent);
}

- (BOOL) parentIsTypeOf_tableView {
    return ([self.parent isKindOfClass: [CCSlider class]]);
}

@end
