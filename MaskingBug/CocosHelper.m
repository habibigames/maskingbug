//
//  CocosHelper.m
//  Colors
//
//  Created by Paul Hammer on 11.12.15.
//  Copyright © 2015 Paul Willy Hammer. All rights reserved.
//

#import "CocosHelper.h"
#import "CCNode.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CCTextureCache+TextureCacheExtension.h"
//#import "HelloWorldScene.h"

@implementation CocosHelper

static NSMutableDictionary *textures;
static UIImage *glowImg_Inner, *glowImg_Outer, *playerBallGlow, *menuButton_glowImg_Outer, *menuButton_glowImg_Inner;

+ (void) preloadTextures {
    if (!textures)
        textures = [[NSMutableDictionary alloc] init];
    [textures removeAllObjects];
    
    [textures setObject:[[CCSprite spriteWithImageNamed:@"playerPad.png"] texture] forKey:@"playerPad.png"];
}

+ (CCTexture*) getTextureWithKey: (NSString*) key {
    if (textures) {
        if ([textures objectForKey:key]) {
            return [textures objectForKey:key];
        }
    }
    return nil;
}

+ (void) addDefault_GradientBackground_ToLayer: (CCScene*) scene withZ:(int)z {
    CCNodeGradient *gradientNode = [CCNodeGradient nodeWithColor:[CCColor colorWithRed:0 green:0 blue:0 alpha:0.3] fadingTo:[CCColor colorWithRed:1 green:1 blue:1 alpha:0.0]];
    [scene addChild:gradientNode z:z];
}

+ (CCNodeGradient*) addDefault_GradientBackground_ToLayer: (CCScene*) scene withOrder: (int) order {
    CCNodeGradient *gradientNode = [CCNodeGradient nodeWithColor:[CCColor colorWithRed:0 green:0 blue:0 alpha:0.3] fadingTo:[CCColor colorWithRed:1 green:1 blue:1 alpha:0.0]];
    [scene addChild:gradientNode z:order];
    return gradientNode;
}

+ (CCNodeColor*) setup_Default_Background_ToLayer: (CCScene*) scene withOrder: (int) order {
    CCNodeColor *background = [CCNodeColor nodeWithColor:[CCColor blackColor]];
    [scene addChild:background z:order];
    return background;
}

+ (CCNodeColor*) setup_GameOver_Background_ToLayer: (CCScene*) scene withOrder: (int) order {
    CCNodeColor *background = [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.949 green:0.059 blue:0.306 alpha:1.0]];
    [scene addChild:background z:order];
    return background;
}

+ (UIImage*)spriteRoundRect:(CGSize)size fill:(ccColor4B)fill outline:(ccColor4B)outline lineWidth:(float)lineWidth {
//    size = CGSizeMake(size.width/2, size.height/2);
    //Mod the size with linewidth
    CGSize modSize = CGSizeMake(size.width-lineWidth*2, size.height-lineWidth*2);
    
    UIGraphicsBeginImageContextWithOptions(modSize, NO, 0.0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBStrokeColor(context, outline.r/255.0, outline.g/255.0, outline.b/255.0, outline.a/255.0);
    CGContextSetRGBFillColor(context, fill.r/255.0, fill.g/255.0, fill.b/255.0, fill.a/255.0);
    
    //Quality Setting - Additional
    //    CGContextSetAllowsAntialiasing(context, YES);
    //    CGContextSetAllowsFontSmoothing(context, YES);
    //    CGContextSetAllowsFontSubpixelQuantization(context, YES);
    //END
    
    CGContextSetLineWidth(context, lineWidth);
    
    //    CGRect  rrect = CGRectMake(lineWidth/2, lineWidth/2, modSize.width, modSize.height);
    CGRect  rrect = CGRectMake(lineWidth/2, lineWidth/2, modSize.width - lineWidth, modSize.height - lineWidth);
    
    CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect), maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect), midy = CGRectGetMidY(rrect), maxy = CGRectGetMaxY(rrect);
    
    CGFloat radius = rrect.size.width/2 + lineWidth/2;
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFillStroke);
    //    CGContextDrawPath(context, kCGPathFill);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    UIImage *scaledImage = [UIImage imageWithCGImage:[image CGImage]
                                               scale:(0.5f)
                                         orientation:(image.imageOrientation)];
//    //Make up a key with the dimensions and colors
//    CCSprite	*newSprite = [CCSprite spriteWithCGImage:[scaledImage CGImage] key:[NSString stringWithFormat:@"RoundRectR:%f:LW%f:%fx%f:%i.%i.%i:%i.%i.%i", radius, lineWidth, size.width, size.height, fill.r, fill.g, fill.b, outline.r, outline.g, outline.b]];
//    return newSprite;
    return scaledImage;
}

+ (CGPoint) getCenter: (CGSize)sz {
    return CGPointMake(sz.width/2, sz.height/2);
}

+ (int) getRndBetween: (int) bottom and: (int) top {
    int rnd = bottom + (arc4random() % (top+1-bottom));
    return rnd;
}

//+ (UIColor*) colorForHexCode: (NSString*) hexCode {
//    NSString *cString = [[hexCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
//    
//    //String should be 6 or 8 characters
//    if ([cString length] < 6)
//        return [UIColor grayColor];
//    
//    // strip 0X if it appears
//    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
//    if ([cString length] != 6) return [UIColor grayColor];
//    
//    //Separate into r,g,b substrings
//    NSRange range;
//    range.location = 0;
//    range.length = 2;
//    NSString *rString = [cString substringWithRange:range];
//    
//    range.location = 2;
//    NSString *gString = [cString substringWithRange:range];
//    
//    range.location = 4;
//    NSString *bString = [cString substringWithRange:range];
//    
//    //Scan Values
//    unsigned int  r, g, b;
//}

+ (UIImage*) createShapeWithSize: (CGSize)size radianDifference: (CGFloat)rdDiff outlineWidth: (CGFloat)oLWidth outlineColor:(UIColor*)outLineColor shadowWidth: (CGFloat) shdwWidth firstColor: (UIColor*) firstColor withFillValue: (float) firstColorFill andSecondColor:(UIColor*) secondColor withFillValue: (float) secondColorFill angle:(float)angle maxBrickAmount: (int) maxBrickAmount {
    CGFloat const kThickness = rdDiff;
    CGFloat const kLineWidth = oLWidth;
    CGFloat const kShadowWidth = shdwWidth;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0); {
        CGContextRef gc = UIGraphicsGetCurrentContext();
//        CGContextAddArc(gc, size.width / 2, size.height / 2,
//                        (size.width - kThickness - kLineWidth) / 2, -M_PI / maxBrickAmount, -(maxBrickAmount-1)*M_PI / maxBrickAmount, NO);
//        float startRadian = degreesToRadians(90) + (3*M_PI)/2 ;
//        float endRadian = degreesToRadians(90) - (3*M_PI)/2;

        float angleMod = angle*(0.75);
        
        CGFloat startRatio = degreesToRadians(-angleMod);
        CGFloat endRatio = degreesToRadians(+angleMod);
        
        // 1. Constants for circle
        CGFloat fullCircle = 2.0 * M_PI;
        CGFloat initialAngle = (3.0 * M_PI_2);
        CGFloat startAngle = startRatio * fullCircle + initialAngle;
        CGFloat endAngle = endRatio * fullCircle + initialAngle;
    
        CGContextAddArc(gc, size.width / 2, size.height / 2,
                        (size.width - kThickness - kLineWidth) / 2, startAngle, endAngle, NO);
        CGContextSetLineWidth(gc, kThickness);
        CGContextSetLineCap(gc, kCGLineCapButt);
        CGContextReplacePathWithStrokedPath(gc);
        CGPathRef path = CGContextCopyPath(gc);
        
        CGContextSetShadowWithColor(gc,
                                    CGSizeMake(0, kShadowWidth / 2), kShadowWidth / 2,
                                    [UIColor colorWithWhite:0 alpha:0.3].CGColor);
        CGContextBeginTransparencyLayer(gc, 0); {
            
            CGContextSaveGState(gc); {
                CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
                CGGradientRef gradient = CGGradientCreateWithColors(rgb, (__bridge CFArrayRef)@[
                                                                                                (__bridge id)firstColor.CGColor,
                                                                                                (__bridge id)secondColor.CGColor
                                                                                                ], (CGFloat[]){ firstColorFill, secondColorFill });
                CGColorSpaceRelease(rgb);
                
                CGRect bbox = CGContextGetPathBoundingBox(gc);
                CGPoint start = bbox.origin;
                CGPoint end = CGPointMake(CGRectGetMaxX(bbox), CGRectGetMaxY(bbox));
                if (bbox.size.width > bbox.size.height) {
                    end.y = start.y;
                } else {
                    end.x = start.x;
                }
                
                CGContextClip(gc);
                CGContextDrawLinearGradient(gc, gradient, start, end, 0);
                CGGradientRelease(gradient);
            } CGContextRestoreGState(gc);
            
            CGContextAddPath(gc, path);
            CGPathRelease(path);
            
            CGContextSetLineWidth(gc, kLineWidth);
            CGContextSetLineJoin(gc, kCGLineJoinMiter);
            [outLineColor setStroke];
            CGContextStrokePath(gc);
            
        } CGContextEndTransparencyLayer(gc);
    }
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage*) createCircleWithSize: (CGSize)size radianDifference: (CGFloat)rdDiff outlineWidth: (CGFloat)oLWidth outlineColor:(UIColor*)outLineColor shadowWidth: (CGFloat) shdwWidth firstColor: (UIColor*) firstColor withFillValue: (float) firstColorFill andSecondColor:(UIColor*) secondColor withFillValue: (float) secondColorFill {
    return [self createCircleWithSizeUsingCGColorRef:size radianDifference:rdDiff outlineWidth:oLWidth outlineColor:outLineColor.CGColor shadowWidth:shdwWidth firstColor:firstColor.CGColor withFillValue:firstColorFill andSecondColor:secondColor.CGColor withFillValue:secondColorFill];
}

+ (UIImage*) createCircleWithSizeUsingCGColorRef: (CGSize)size radianDifference: (CGFloat)rdDiff outlineWidth: (CGFloat)oLWidth outlineColor:(CGColorRef)outLineColor shadowWidth: (CGFloat) shdwWidth firstColor: (CGColorRef) firstColor withFillValue: (float) firstColorFill andSecondColor:(CGColorRef) secondColor withFillValue: (float) secondColorFill {
    CGFloat const kThickness = rdDiff;
    CGFloat const kLineWidth = oLWidth;
    CGFloat const kShadowWidth = shdwWidth;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0); {
        CGContextRef gc = UIGraphicsGetCurrentContext();
        
        // 1. Constants for circle
        const CGFloat startAngle = -((float)M_PI / 2);
        const CGFloat endAngle = ((2 * (float)M_PI) + startAngle);
        
        CGContextAddArc(gc, size.width / 2, size.height / 2,
                        (size.width - kThickness - kLineWidth) / 2, startAngle, endAngle, NO);
        CGContextSetLineWidth(gc, kThickness);
        CGContextSetLineCap(gc, kCGLineCapButt);
        CGContextReplacePathWithStrokedPath(gc);
        CGPathRef path = CGContextCopyPath(gc);
        
        CGContextSetShadowWithColor(gc,
                                    CGSizeMake(0, kShadowWidth / 2), kShadowWidth / 2,
                                    [UIColor colorWithWhite:0 alpha:0.3].CGColor);
        CGContextBeginTransparencyLayer(gc, 0); {
            
            CGContextSaveGState(gc); {
                CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
                CGGradientRef gradient = CGGradientCreateWithColors(rgb, (__bridge CFArrayRef)@[
                                                                                                (__bridge id)firstColor,
                                                                                                (__bridge id)secondColor
                                                                                                ], (CGFloat[]){ firstColorFill, secondColorFill });
                CGColorSpaceRelease(rgb);
                
                CGRect bbox = CGContextGetPathBoundingBox(gc);
                CGPoint start = bbox.origin;
                CGPoint end = CGPointMake(CGRectGetMaxX(bbox), CGRectGetMaxY(bbox));
                if (bbox.size.width > bbox.size.height) {
                    end.y = start.y;
                } else {
                    end.x = start.x;
                }
                
                CGContextClip(gc);
                CGContextDrawLinearGradient(gc, gradient, start, end, 0);
                CGGradientRelease(gradient);
            } CGContextRestoreGState(gc);
            
            CGContextAddPath(gc, path);
            CGPathRelease(path);
            
            CGContextSetLineWidth(gc, kLineWidth);
            CGContextSetLineJoin(gc, kCGLineJoinMiter);
            [[UIColor colorWithCGColor:outLineColor] setStroke];
            CGContextStrokePath(gc);
            
        } CGContextEndTransparencyLayer(gc);
    }
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark collision handler
//Returns true if the circles are touching, or false if they are not
+ (bool) circlesCollidingPosition1:(CGPoint)pos1 radius1:(float)rd1 position2:(CGPoint)pos2 radius2:(float)rd2
{
    //compare the distance to combined radii
    float dx = pos2.x - pos1.x;
    float dy = pos2.y - pos1.y;
    float radii = rd1 + rd2;
    if ( ( dx * dx )  + ( dy * dy ) < radii * radii )
    {
        return true;
    }
    else
    {
        return false;
    }
}

#pragma mark Color Handling
+ (UIColor*) colorForColorType: (int) cT {
    NSString *colorHexString = color_0;
    switch (cT) {
        case colorType_Green:
            colorHexString = color_1;
            break;
        case colorType_Yellow:
            colorHexString = color_2;
            break;
        case colorType_Orange:
            colorHexString = color_3;
            break;
        case colorType_Pink:
            colorHexString = color_4;
            break;
    }
    return [UIColor colorWithHexString:colorHexString];
}

#pragma mark Glow Effect for Sprites
+ (void)addGlowEffect:(CCSprite*)sprite color:(CCColor*)color size:(CGSize)size{
    
    CGPoint pos = ccp(sprite.contentSize.width / 2,
                      
                      sprite.contentSize.height / 2);
    
    
    
//    CCSprite* glowSprite = [CCSprite spriteWithSpriteFrame:sprite.spriteFrame];
    CCSprite* glowSprite = [CCSprite spriteWithTexture:sprite.texture];
    
    [glowSprite setColor:color];
    
    [glowSprite setPosition:pos];
    
    glowSprite.rotation = sprite.rotation;
    
    
    
//    struct _ccBlendFunc f = {GL_ONE, GL_ONE};
    
//    glowSprite.blendFunc = f;
//    [glowSprite setBlendMode:[CCBlendMode blendModeWithOptions:@{CCBlendFuncSrcAlpha:@(gl),CCBlendFuncDstAlpha:@(GL_ONE)}]];
//    [glowSprite setBlendMode:[CCBlendMode blendModeWithOptions:@{CCBlendFuncSrcAlpha:@(GL_SRC_ALPHA),CCBlendFuncDstAlpha:@(GL_ONE),}]];
    
    glowSprite.blendMode = [CCBlendMode blendModeWithOptions:@{
                                                                         CCBlendFuncSrcColor: @(GL_ONE),
                                                                         CCBlendFuncDstColor: @(GL_ONE),
                                                                         }];
    
    [sprite addChild:glowSprite z:-1];
    
    
    
    // Run some animation which scales a bit the glow
    
    CCActionSequence * s1 = [CCActionSequence actions:[CCActionScaleTo actionWithDuration:.9f scale:1.0],[CCActionScaleTo actionWithDuration:.9f scale:1.09], nil];
    
    /* CCSequence* s1 = CCSequence::actionOneTwo(
     
     CCScaleTo::actionWithDuration(0.9f, size.width, size.height),
     
     CCScaleTo::actionWithDuration(0.9f, size.width*0.75f, size.height*0.75f));*/
    
    CCActionRepeatForever *r = [CCActionRepeatForever actionWithAction:s1];
    [glowSprite runAction:r];
}

#pragma mark Convert methods
+ (UIImage *) renderUIImageFromSprite:  (CCSprite *)sprite {
    
    int tx = sprite.contentSize.width*[UIScreen mainScreen].scale;
    int ty = sprite.contentSize.height*[UIScreen mainScreen].scale;
    
    CCRenderTexture *renderer    = [CCRenderTexture renderTextureWithWidth:tx height:ty];
    
    sprite.anchorPoint  = CGPointZero;
    
    [renderer begin];
    [sprite visit];
    [renderer end];
    
    return [renderer getUIImage];
}

@end
