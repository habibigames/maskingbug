uniform mat4 CC_PMatrix;
uniform mat4 CC_MVPMatrix;

uniform sampler2D u_MaskTexture;
//uniform vec2 u_MaskTextureSize; // content size in points
//uniform vec2 u_MaskTexturePixelSize; // content size in pixels

void main()
{
    //CC_PMatrix is the projection matrix, where as the CC_MVPMatrix is the model, view, projection matrix. Since in 2d we are using ortho camera CC_PMatrix is enough to do calculations.
    //gl_Position = CC_MVPMatrix * a_position;
    
//    vec4 totalOffset = vec4(u_MaskTexturePixelSize.x / 2.0, u_MaskTexturePixelSize.y / 2.0, 0.0, 0.0);
    
//    gl_Position     = CC_PMatrix * ( cc_Position - totalOffset );
    //    gl_Position = cc_Position + vec4(0.5,0.5,0,0);
    vec4 pos  = CC_MVPMatrix * cc_Position;
//    pos.x+= 1.0;
//    pos.x+= 1.0;

//    pos.y*= 0.5;
    pos.y+= 0.0;
//    pos.y+= 0.75;
//    pos.t += 0.5
    gl_Position = pos;
    
    
    
//    gl_Position     = CC_PMatrix * cc_Position;
    cc_FragColor    = clamp(cc_Color, 0.0, 1.0);
    
//    vec4 coorAddition = cc_TexCoord1;
//    coorAddition.x += 0.5;
//    coorAddition.y += 0.25;
//    
//    
//    vec4 coorAddition2 = cc_TexCoord2;
//    coorAddition2.x += 0.5;
//    coorAddition2.y += 0.25;
    
    
//    cc_FragTexCoord1= coorAddition;// + vec4(1,1,0,0);
//    cc_FragTexCoord2= coorAddition2;

    cc_FragTexCoord1 = cc_TexCoord1;
    cc_FragTexCoord2 = cc_TexCoord2;
    
    
//    cc_FragTexCoord1 = vec2(cc_TexCoord1.s, 1.0 - cc_TexCoord1.t);
//    cc_FragTexCoord2 = vec2(cc_TexCoord2.s, 1.0 - cc_TexCoord2.t);
}