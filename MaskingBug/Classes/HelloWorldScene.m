//
//  HelloWorldScene.m
//
//  Created by : Paul Hammer
//  Project    : MaskingBug
//  Date       : 29.06.16
//
//  Copyright (c) 2016 Paul Willy Hammer.
//  All rights reserved.
//
// -----------------------------------------------------------------

#import "HelloWorldScene.h"
#import "CCTextureCache.h"
#import "CE_Label.h"
#import "CEMaskedContainer.h"
#import "CCToggleButton_OptionsButton.h"
#import "CCEffectNode_FadedBorder_CE.h"

#define shaderActivated true

// -----------------------------------------------------------------------

@implementation HelloWorldScene {
    CCTableView *tableView;
    NSMutableArray<CCNode*> *tableItems;
    CCEffectNode_FadedBorder_CE *mCnt;
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    
    // The thing is, that if this fails, your app will 99.99% crash anyways, so why bother
    // Just make an assert, so that you can catch it in debug
    NSAssert(self, @"Whoops");
    
    [self setAnchorPoint:ccp(0.5,0.5)];
    
    [self setupGame];
    
    // done
    return self;
}

// -----------------------------------------------------------------------

- (void) setupGame {
    
    tableView = [self setupTable];
    
    const float originalScale = tableView.scale;
    [tableView setScale:0.0];
    [tableView runAction:[CCActionSequence actions:[CCActionDelay actionWithDuration:1],[CCActionScaleTo actionWithDuration:0.5 scale:originalScale], nil]];
    
//    [self createAnnotationsView];
    
    [self setPosition:[self getCenterForSize:self.contentSizeInPoints]];
}

- (CGPoint) getCenterForSize: (CGSize) sz {
    return ccp( ( sz.width > 0  ? sz.width  /2.0 : 0 ) ,
                ( sz.height > 0 ? sz.height /2.0 : 0 ));
}

- (CCTableView*) setupTable {
    
//    CGPoint loc = [self getCenterForSize: self.contentSize];
    CGRect rect = CGRectMake(self.contentSizeInPoints.width/2.0, self.contentSizeInPoints.height/2.0, self.contentSizeInPoints.width * 0.7, self.contentSizeInPoints.height * 0.8);
    
    CCColor *textColor = [CCColor blueColor];
    
    const float width = rect.size.width;
    
    //Setup Array for table items
    tableItems = [NSMutableArray<CCNode*> array];

    [self setupItem_withInfo:@"ROUNDS PLAYED:" andScoreString:@"123" maxWidth:width andColor:textColor andImageName:@""];
    
    [self addLine: rect.size.width andColor:textColor];
    
    [self setupItem_withInfo:@"HIGHSCORE:" andScoreString:@"999" maxWidth:width andColor:textColor andImageName:@""];
    
    [self setupItem_withInfo:@"POINTS TOTAL:" andScoreString:@"1283" maxWidth:width andColor:textColor andImageName:@""];
    
    [self switchWithTitle:@"MUSIC: " selector:@selector(switchToggle) maxWidth:width enabled:TRUE fontSize:defaultFontSize];
    
    [self setupSensitivitySlider_Own:rect.size.width];
    
    [self addLine: rect.size.width andColor:textColor];
    
    [self setupItem_withInfo:@"LEVELS SOLVED:" andScoreString:@"5/27" maxWidth: width andColor:textColor andImageName:@""];
    
    CCTableView *table = [CocosHelper create CGSizeMake(width, rect.size.height)];
    [table reloadData];
    
    return table;
}

- (CCToggleButton_OptionsButton*) switchWithTitle: (NSString*) str selector: (SEL) selector maxWidth: (float) maxWidth enabled:(BOOL) isEnabled fontSize: (float) fontSize {
    CCNode* sound = [CCNode node];
    float width = maxWidth;
    [sound setContentSize:CGSizeMake(width, table_minRowHeight)];
    
    CCColor *clr = [CCColor colorWithUIColor:[CocosHelper colorForColorType:colorType_Cyan]];
    
    CGRect rect = sound.boundingBox;
    //Add Score Label Here
    CCLabelTTF *lbl = [CCLabelTTF labelWithString:str fontName:@"RussellSquare" fontSize:fontSize];
    [lbl setContentSizeInPoints:rect.size];
    [lbl setColorRGBA:clr];
    
    [sound addChild:lbl z:Z_GAMEOVER_Labels];
    
    [lbl setAnchorPoint:ccp(0.0, 0.5)];
    //    [lbl_title setPosition:ccp(0, lbl_title.position.y - (rect.size.height/20))];
    //    [lbl setPosition:ccp(0.5f, 0.5f)];
    [lbl setHorizontalAlignment:CCTextAlignmentLeft];
    [lbl setVerticalAlignment:CCVerticalTextAlignmentCenter];
    [lbl setAdjustsFontSizeToFit:TRUE];
    [lbl setMinimumFontSize:10];
    [lbl setShadowOffset:CGPointZero];
    [lbl setShadowColor:clr];
    [lbl setShadowBlurRadius:10];
    [lbl setPosition:ccp(sound.contentSizeInPoints.width*0.00025f,lbl.contentSize.height/2 - lbl.contentSize.height/6)];
    
    //    [lbl setDimensions:dimensions];
    
    
    CCToggleButton_OptionsButton *switchControl = [CCToggleButton_OptionsButton buttonWithStatusEnabled:isEnabled onTargetClass:self andSelectorAction:selector
//                                                                                         andContentSize:CGSizeMake(60, table_minRowHeight)
                                                  preferedSizeInPoints:CGSizeMake(60, 30) ];
    [sound addChild:switchControl];
    [switchControl setPositionInPoints:ccp(sound.contentSize.width * xAxisPosForSwitch + switchControl.contentSizeInPoints.width/2,
                                           -switchControl.contentSizeInPoints.height
                                           )];
    [tableItems addObject:sound];
    
    return switchControl;
}
const float xAxisPosForSwitch = 0.65;

- (void) switchToggle {
    
}

- (void) setupSensitivitySlider_Own: (float) maxWidth {
    CCNode* sound = [CCNode node];
    float width = maxWidth;
    [sound setContentSize:CGSizeMake(width, table_minRowHeight)];
    
    CCColor *clr = [CCColor colorWithUIColor:[CocosHelper colorForColorType:colorType_Cyan]];
    
    CGRect rect = sound.boundingBox;
    //Add Score Label Here
    CCLabelTTF *lbl = [CCLabelTTF labelWithString:@"SENSITIVITY:" fontName:@"RussellSquare" fontSize:menu_SwitchButton_Label_FontSize];
    [lbl setContentSizeInPoints:rect.size];
    [lbl setColorRGBA:clr];
    
    [sound addChild:lbl z:Z_GAMEOVER_Labels];
    
    [lbl setAnchorPoint:ccp(0.0, 0.5)];
    //    [lbl_title setPosition:ccp(0, lbl_title.position.y - (rect.size.height/20))];
    //    [lbl setPosition:ccp(0.5f, 0.5f)];
    [lbl setHorizontalAlignment:CCTextAlignmentLeft];
    [lbl setVerticalAlignment:CCVerticalTextAlignmentCenter];
    [lbl setAdjustsFontSizeToFit:TRUE];
    [lbl setMinimumFontSize:10];
    [lbl setShadowOffset:CGPointZero];
    [lbl setShadowColor:clr];
    [lbl setShadowBlurRadius:10];
    [lbl setPosition:ccp(sound.contentSizeInPoints.width*0.00025f,lbl.contentSize.height/2 - lbl.contentSize.height/6)];
    
    //    [lbl setDimensions:dimensions];
    
    //Value changes will be handled inside the CESlider_Sensitivity Class
    sliderCrtl = [[CESliderControl alloc] initWithBackground:[CCSpriteFrame frameWithImageNamed:slidersensitivity] andHandleImage:[CCSpriteFrame frameWithImageNamed:slidersensitivity_ball]];
    [sound addChild:sliderCrtl];
    [sliderCrtl setAnchorPoint:ccp(0.0f, 0.5f)];
    
    [sliderCrtl setPositionInPoints:ccp(sound.contentSizeInPoints.width/2.0, sound.contentSizeInPoints.height/2.0 + sliderCrtl.contentSizeInPoints.height/2.0)];
    //    [sensitivitySlider toggleOn:[[SaveGame getInstance] sv_boolForKey:SaveGame_music_onoff]];
    [sliderCrtl setPosition:ccp(sound.contentSize.width/2.0f - ((sliderCrtl.contentSizeInPoints.width*sliderCrtl.scale/sliderCrtl.background.contentSizeInPoints.width)*12.5f),
                                           (sliderCrtl.contentSize.height*sliderCrtl.scale)/2.0f
                                           )];
    [sliderCrtl setContinuous:YES];
    
    [sliderCrtl setScale:(maxWidth/2.0)/sliderCrtl.contentSizeInPoints.width];
    
    [sliderCrtl setUserInteractionEnabled:YES];
    [sliderCrtl setClaimsUserInteraction:YES];

    //    [sliderCrtl setScrollViewForCustomHandling:tableView];
    //    [sensitivitySlider]
    //    [sensitivitySlider sete]
    
    [tableItems addObject:sound];
}

- (void) addLine: (float) width andColor: (CCColor*) clr {
    CCNode *node = [CCNode node];
    [node setContentSize:CGSizeMake(width, 10)];
    [node setPositionType:CCPositionTypeNormalized];
    [node setAnchorPoint:ccp(0.0,-1.25)];
    [node setPosition:ccp(0.0,0)];
    
    CCNodeColor *line = [CCNodeColor nodeWithColor:clr];
    [line setContentSize:CGSizeMake(node.boundingBox.size.width, 1.0f)];
    [node addChild:line];
    [line setAnchorPoint:ccp(0.0, 0.0)];
    
    [tableItems addObject:node];
}

- (void) setupItem_withInfo: (NSString*) inf andScoreString: (NSString*) scoreTxt maxWidth: (float) width andColor: (CCColor*) clr andImageName: (NSString*) imgName {
    CCNode* sound = [CCNode node];
    [sound setContentSize:CGSizeMake(width, table_minRowHeight)];
    [sound setPositionType:CCPositionTypeNormalized];
    [sound setAnchorPoint:ccp(0.0,0)];
    [sound setPosition:ccp(0.0,0.0)];
    
    CGRect rect = sound.boundingBox;
    
    //Add Score Label Here
    CCLabelTTF *lbl = [CCLabelTTF labelWithString:inf fontName:@"RussellSquare" fontSize:defaultFontSize];
    [lbl setContentSizeInPoints:rect.size];
    [lbl setColorRGBA:clr];
    
    [sound addChild:lbl];
    
    [lbl setAnchorPoint:ccp(0,0)];
    [lbl setHorizontalAlignment:CCTextAlignmentLeft];
    [lbl setVerticalAlignment:CCVerticalTextAlignmentCenter];
    [lbl setAdjustsFontSizeToFit:TRUE];
    [lbl setShadowOffset:CGPointZero];
    [lbl setShadowColor:clr];
    [lbl setShadowBlurRadius:10];
    [lbl setPosition:ccp((-sound.contentSizeInPoints.width)*0.025,0)];
    
    //Add Score Label Here
    CE_Label *lblScore = [CE_Label labelWithFontName:@"RussellSquare" fontSize: defaultFontSize text:scoreTxt andGlyphName:imgName andContentSize:rect.size];
    
    [lblScore setContentSizeInPoints:rect.size];
    [lblScore setColorRGBA:clr];
    [lblScore setAnchorPoint:ccp(0, 0)];
    
    [sound addChild:lblScore];
    
    [lblScore.textLabel setHorizontalAlignment:CCTextAlignmentLeft];
    [lblScore.textLabel setVerticalAlignment:CCVerticalTextAlignmentCenter];
    [lblScore.textLabel setAdjustsFontSizeToFit:TRUE];
    [lblScore.textLabel setShadowOffset:CGPointZero];
    [lblScore.textLabel setShadowColor:clr];
    [lblScore.textLabel setShadowBlurRadius:10];
    [lblScore needsLayout];
    
    [lblScore setPosition:ccp(sound.contentSizeInPoints.width*0.65f,0)];
    
    [tableItems addObject:sound];
}

- (void) setupItem_withLinkText: (NSString*) inf andScoreString: (NSString*) scoreTxt maxWidth: (float) width andColor: (CCColor*) clr andImageName: (NSString*) imgName {
    CCNode* sound = [CCNode node];
    [sound setContentSize:CGSizeMake(width, table_minRowHeight)];
    [sound setPositionType:CCPositionTypeNormalized];
    [sound setAnchorPoint:ccp(0.0,0)];
    [sound setPosition:ccp(0.0,0.0)];
    
    CGRect rect = sound.boundingBox;
    
    //Add Score Label Here
    CCLabelTTF *lbl = [CCLabelTTF labelWithString:inf fontName:@"RussellSquare" fontSize:defaultFontSize];
    [lbl setContentSizeInPoints:rect.size];
    [lbl setColorRGBA:clr];
    
    [sound addChild:lbl];
    
    [lbl setAnchorPoint:ccp(0,0)];
    [lbl setHorizontalAlignment:CCTextAlignmentLeft];
    [lbl setVerticalAlignment:CCVerticalTextAlignmentCenter];
    [lbl setAdjustsFontSizeToFit:TRUE];
    [lbl setShadowOffset:CGPointZero];
    [lbl setShadowColor:clr];
    [lbl setShadowBlurRadius:10];
    [lbl setPosition:ccp((-sound.contentSizeInPoints.width)*0.025,0)];
    
    //Add Score Label Here
    CE_Label *lblScore = [CE_Label labelWithFontName:@"RussellSquare" fontSize: defaultFontSize text:scoreTxt andGlyphName:imgName andContentSize:rect.size];
    
    [lblScore setContentSizeInPoints:rect.size];
    [lblScore setColorRGBA:clr];
    [lblScore setAnchorPoint:ccp(0, 0)];
    
    [sound addChild:lblScore];
    
    [lblScore.textLabel setHorizontalAlignment:CCTextAlignmentLeft];
    [lblScore.textLabel setVerticalAlignment:CCVerticalTextAlignmentCenter];
    [lblScore.textLabel setAdjustsFontSizeToFit:TRUE];
    [lblScore.textLabel setShadowOffset:CGPointZero];
    [lblScore.textLabel setShadowColor:clr];
    [lblScore.textLabel setShadowBlurRadius:10];
    [lblScore needsLayout];
    
    [lblScore setPosition:ccp(sound.contentSizeInPoints.width*0.65f,0)];
    
    [tableItems addObject:sound];
}


#pragma mask Transparence Mask Handling
- (CCTexture*) createTransparencyTextureMaskWithSize: (CGSize) sz {
    const float gradientBorderHeight    = 60.0f;
    float width     = sz.width;
    float height    = sz.height;
    
    //Colors
    CCColor *fadingFromClr  = [CCColor whiteColor];
    CCColor *fadingToClr    = [CCColor colorWithWhite:1 alpha:0];
    
    
    //Container Node for colored Nodes
    CCSprite *scissorRect   = [[CCSprite alloc] init];
    [scissorRect            setContentSizeInPoints:CGSizeMake(width, height)];
    
    //Color node in the middle of the following two nodes
    CCNodeColor *clrNode = [CCNodeColor nodeWithColor:fadingFromClr width:width height:height-gradientBorderHeight*2.0f];
    
    [clrNode    setContentSizeInPoints:CGSizeMake(width, height - gradientBorderHeight*2.0f)];
    
    [clrNode    setAnchorPoint:ccp(0.5, 0.5)];
    
    [scissorRect addChild:clrNode z:1];
    
    [clrNode    setPositionType:CCPositionTypeNormalized];
    
    [clrNode    setPosition:ccp(0.5f, 0.5f)];
    
    //Gradient node on top of container node
    CCNodeGradient *gradient_top
    = [CCNodeGradient nodeWithColor: fadingFromClr fadingTo: fadingToClr alongVector:ccp(0, -1)];
    
    [gradient_top   setContentSizeInPoints:CGSizeMake(width, gradientBorderHeight)];
    
    [scissorRect    addChild:gradient_top z:1];
    
    [gradient_top   setAnchorPoint:ccp(0.5, 1)];
    
    [gradient_top   setPositionType:CCPositionTypeNormalized];
    
    [gradient_top   setPosition:ccp(0.5f, 1.0f)];
    
    
    
    //Gradient node on bottom of container node
    CCNodeGradient *gradient_bottom
    = [CCNodeGradient nodeWithColor: fadingFromClr fadingTo: fadingToClr alongVector:ccp(0, 1)];
    
    [gradient_bottom setContentSizeInPoints:CGSizeMake(width, gradientBorderHeight)];
    
    [scissorRect addChild:gradient_bottom z:1];
    
    [gradient_bottom setAnchorPoint:ccp(0.5, 0)];
    
    [gradient_bottom setPositionType:CCPositionTypeNormalized];
    
    [gradient_bottom setPosition:ccp(0.5f, 0.0f)];
    

    //Set AnchorPoint and Position of Container node
    [scissorRect    setAnchorPoint: ccp(0.0, 0.0)];
    [scissorRect    setPosition:    ccp(0, 0)];
    
    CCRenderTexture *rend   = [CCRenderTexture renderTextureWithWidth:width height:height];
    [rend beginWithClear:   0 g:0 b:0 a:0];
    [scissorRect            visit];
    [rend end];
    
    //comment out the following for displaying the mask
    //    CGRect rect     = self.boundingBox;
    //    [self addChild: scissorRect];
    //    [scissorRect    setPosition:ccp(rect.size.width/2, rect.size.height/2)];
    
    //    return rend.texture;
    return [[CCTextureCache sharedTextureCache] addCGImage:rend.getUIImage.CGImage
                                                    forKey:@"transparencyMaskBottonNTop"];
}

///**
// *  This method creates a Table --> its screen output will be masked by a mask
// *
// *  @param sz ContentSize of the node in Points
// *
// *  @return MaskedTableView --> its parent is a CCRenderTexture
// */
//- (CCTableView*) createMaskedTableWithSize3: (CGSize) sz {
//    // Setup Table
//    CCTableView *table =  [CCTableView node];
//    table.dataSource = self;
//    
//    [table setRowHeight:table_minRowHeight];
//    [table setAnchorPoint:      ccp(0.5, 0.5)];
//    [table setContentSizeType:  CCSizeTypeNormalized];
//    [table setContentSize:      CGSizeMake( sz.width / self.contentSizeInPoints.width, 1.0)];
//    [table setPositionType:     CCPositionTypeMake(CCPositionUnitNormalized, CCPositionUnitNormalized, CCPositionReferenceCornerBottomLeft) ];
//    [table setPosition:         ccp(0.5, 0.5)];
//    
//    mCnt = [CCEffectNode_FadedBorder_CE containerWithWidth:self.contentSizeInPoints.width height:sz.height];
//    
////    CCEffectNode *effectNode  = [CCEffectNode effectNodeWithWidth:self.contentSizeInPoints.width height:sz.height pixelFormat:CCTexturePixelFormat_RGBA8888];
//    
//    [self addChild:mCnt];
//    [mCnt addChild:table];
//    
//    return table;
//}
//
/////**
//// *  This method creates a Table --> its screen output will be masked by a mask
//// *
//// *  @param sz ContentSize of the node in Points
//// *
//// *  @return MaskedTableView --> its parent is a CCRenderTexture
//// */
////- (CCTableView*) createMaskedTableWithSize2: (CGSize) sz {
////    // Setup Table
////    CCTableView *table =  [CCTableView node];
////    table.dataSource = self;
////    
////    [table setRowHeight:table_minRowHeight];
////    
////    [table setAnchorPoint:      ccp(0.5, 0.5)];
////    
////    [table setContentSizeType:  CCSizeTypeNormalized];
////    [table setContentSize:      CGSizeMake( sz.width / self.contentSizeInPoints.width, 1.0)];
////    
////    [table setPositionType:     CCPositionTypeMake(CCPositionUnitNormalized, CCPositionUnitNormalized, CCPositionReferenceCornerBottomLeft) ];
////    [table setPosition:         ccp(0.5, 0.5)];
////    
////    mCnt = [CEMaskedContainer containerWithWidth:self.contentSizeInPoints.width height:sz.height];
////    
////    CCEffectNode *effectNode  = [CCEffectNode effectNodeWithWidth:self.contentSizeInPoints.width height:sz.height pixelFormat:CCTexturePixelFormat_RGBA8888];
////    CCTexture *mask = [mCnt createTransparencyTextureMaskWithSize:CGSizeMake((int)effectNode.contentSizeInPoints.width, (int)effectNode.contentSizeInPoints.height) fadeEffectOnBottom:TRUE onTop:TRUE];
////    
////    [effectNode.sprite shaderUniforms]  [@"u_MaskTexture"]    = mask;
////    [effectNode.sprite setShader:       [CCShader shaderNamed:@"MaskPositive"]];
////    [effectNode setClearColor:          [CCColor clearColor]];
////    [effectNode setClearFlags:          (GLbitfield) GL_COLOR_BUFFER_BIT];
////    [effectNode setAutoDraw:            TRUE];
////    
////    [self addChild:effectNode];
////    [effectNode addChild:table];
//////    [effectNode setPositionInPoints:[CocosHelper getCenter: effectNode.parent.contentSizeInPoints]];
//////    [self addChild:mCnt];
//////    [mCnt addChild:table];
////    [mCnt updatePosition];
//////    [mCnt setPositionInPoints:ccp(mCnt.positionInPoints.x, self.contentSizeInPoints.height)];
////    
////    return table;
////}

/**
 *  This method creates a Table --> its screen output will be masked by a mask
 *
 *  @param sz ContentSize of the node in Points
 *
 *  @return MaskedTableView --> its parent is a CCRenderTexture
 */
- (CCTableView*) createMaskedTableWithSize: (CGSize) sz {
    
    // Generate mask for table
    CCTexture *maskTex = [self createTransparencyTextureMaskWithSize:sz];
    
    // Setup Table
    CCTableView *table =  [CCTableView node];
    table.dataSource = self;
    
    [table setRowHeight:table_minRowHeight];
    
    [table setAnchorPoint:      ccp(0.5, 0.5)];
    
    [table setContentSizeType:  CCSizeTypeNormalized];
    [table setContentSize:      CGSizeMake( 1.0, 1.0)];
    
    [table setPositionType:     CCPositionTypeMake(CCPositionUnitNormalized, CCPositionUnitNormalized, CCPositionReferenceCornerBottomLeft) ];
    [table setPosition:         ccp(0.5, 0.5)];
    
    
    
    CCRenderTexture *renderer = [CCRenderTexture renderTextureWithWidth:(int) sz.width height:(int) sz.height];
    
    //Add table to renderer node
    [renderer addChild:table];
    
    if (shaderActivated) {
    [[renderer sprite] shaderUniforms][@"u_MaskTexture"]    = maskTex;
    
    //    [renderer setBlendMode: [CCBlendMode blendModeWithOptions:@{
    //                                          CCBlendFuncSrcAlpha: @(GL_ZERO),
    //                                          CCBlendFuncDstAlpha: @(GL_ONE_MINUS_SRC_ALPHA),
    //                                         CCBlendEquationAlpha: @(GL_FUNC_REVERSE_SUBTRACT),
    //
    //                                          }]];
    
    [[renderer sprite] setShader:   [CCShader shaderNamed:@"MaskPositive"]];
    [renderer setClearColor:        [CCColor clearColor]];
        [renderer setClearFlags:        (GLbitfield) GL_COLOR_BUFFER_BIT];
        [renderer setAutoDraw:          TRUE];
    }
    
    [renderer setAnchorPoint:       ccp(0.5f, 0.5f)];
//    [renderer setPositionType:      CCPositionTypeMake(CCPositionUnitNormalized, CCPositionUnitNormalized, CCPositionReferenceCornerBottomLeft)];
//    [renderer setPosition:          ccp(0.5, 0.5)];

    [renderer setPositionInPoints:[self getCenterForSize:self.contentSizeInPoints]];
    
    [self addChild:renderer];
        
    return table;
}

#pragma mark TableView Methods
- (CCTableViewCell*) tableView:(CCTableView*)tableView nodeForRowAtIndex:(NSUInteger) index {
    if (tableItems.count <= 0) return nil;
    
    CCTableViewCell* cell   = [CCTableViewCell node];

    cell.contentSize        = [tableItems objectAtIndex:index].contentSize;
    
    [cell addChild:           [tableItems objectAtIndex:index]];
    
    [cell setAnchorPoint:      ccp(0.5, 1)];
    
    return cell;
}

- (float) tableView:(CCTableView *)tableView heightForRowAtIndex:(NSUInteger)index {
    if (!tableItems || tableItems.count -1 < index) return 0.0f;
    else return [tableItems objectAtIndex:index].contentSize.height;
}

- (NSUInteger) tableViewNumberOfRows:(CCTableView*) tableView {
    return (tableItems.count > 0)?tableItems.count:0; // just a demo
}

#pragma mark annotations masking test
- (void) createAnnotationsView {
    CGSize sz = self.contentSizeInPoints;
        
    maskedContainer_AnnotationsView         = [CEAnnotationsView annotationViewWithWidth:sz.width height:sz.height andCircleRect:CGRectMake(sz.width/2.0, sz.height/2.0, sz.width*0.8, sz.width*0.8) /*TODO for real project: use padControl Rect*/];
    
    //Add MaskedContainerView including Annotations View to Main Scene
    [self addChild:maskedContainer_AnnotationsView];
    
    [maskedContainer_AnnotationsView updatePosition];
    
    [self annotationTest];
}

- (void)annotationTest { //Use this method only for testing the notification banner for achievements
    if( annotationTestsEnabled ) { // Ony execute if achievement banner test definition equals true
        const NSArray *achvs = [NSArray arrayWithObjects:
                                
                                [NSNumber numberWithInt:CEAT_1_1_1],
                                [NSNumber numberWithInt:CEAT_1_1_2],
                                [NSNumber numberWithInt:CEAT_1_1_3],
                                
                                [NSNumber numberWithInt:CEAT_1_1_1],
                                [NSNumber numberWithInt:CEAT_1_1_2],
                                [NSNumber numberWithInt:CEAT_1_1_3],
                                [NSNumber numberWithInt:CEAT_1_1_1],
                                [NSNumber numberWithInt:CEAT_1_1_2],
                                [NSNumber numberWithInt:CEAT_1_1_3],
//                                [NSNumber numberWithInt:CEAT_2_1_1],
                                
                                
                                 nil];
        
        __block NSUInteger achvsIndex = -1;
        
        CCActionCallBlock *blockRunNotification = [CCActionCallBlock actionWithBlock:^{
            achvsIndex++;
            if (achvsIndex < [achvs count]) {
                CEAnnotationItem_AnimationType aT = (CEAnnotationItem_AnimationType) [[achvs objectAtIndex:achvsIndex] intValue];
                [maskedContainer_AnnotationsView generateNewAnnotation:aT];
            }
        }];
        
        [self runAction:[CCActionRepeat actionWithAction:
                         
                         [CCActionSequence actions:
                          
                          blockRunNotification,
                          
                          [CCActionDelay actionWithDuration: [CocosHelper getRndBetween:10 and:40]/10.0]
                          
                          , nil]
                         
                                                   times:[achvs count]]];
        
    }
    
    CGSize cntSz = maskedContainer_AnnotationsView.contentSizeInPoints;
    
    CCNodeColor *clN = [CCNodeColor nodeWithColor:[CCColor whiteColor] width:cntSz.width height:cntSz.height];
//    clN.positionInPoints = ccp(cntSz.width/2.0, cntSz.height/2.0);
    [maskedContainer_AnnotationsView addChild:clN];
}


@end























// why not add a few extra lines, so we dont have to sit and edit at the bottom of the screen ...
