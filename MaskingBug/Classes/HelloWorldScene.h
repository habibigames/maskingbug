//
//  HelloWorldScene.h
//
//  Created by : Paul Hammer
//  Project    : MaskingBug
//  Date       : 29.06.16
//
//  Copyright (c) 2016 Paul Willy Hammer.
//  All rights reserved.
//
// -----------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "cocos2d-ui.h"
#import "CESliderControl.h"


#define table_minRowHeight  35.0f
#define defaultFontSize     12.0f

// -----------------------------------------------------------------------

@interface HelloWorldScene : CCScene <CCTableViewDataSource> {
    
    //Annotations
    CEAnnotationsView *maskedContainer_AnnotationsView;
    
    CESliderControl *sliderCrtl;
}

// -----------------------------------------------------------------------

- (instancetype)init;

// -----------------------------------------------------------------------

@end


































