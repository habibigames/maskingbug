//
//  CE_Label.m
//  VERTEX
//
//  Created by Paul Hammer on 24.05.16.
//  Copyright © 2016 ColossalEntertainment. All rights reserved.
//

#import "CE_Label.h"

@implementation CE_Label {
    CCLabelTTF *lbl;
    CCSprite *imgS;
}

+ (instancetype) labelWithFontName: (NSString*) fontName fontSize: (float) fontSize text: (NSString*) text andGlyphName: (NSString*) glyphName andContentSize: (CGSize) cntSize {
    CE_Label *this = [[CE_Label alloc] init];
    [this setContentSize:cntSize];
    [this setDirection:CCLayoutBoxDirectionHorizontal];
    [this setSpacing:-16];
    [this setupWithFontName:fontName fontSize:fontSize text:text andGlyphName:glyphName];
    [this needsLayout];
    return this;
}

- (void) setupWithFontName: (NSString*) fontName fontSize: (float) fontSize text: (NSString*) text andGlyphName: (NSString*) glyphName {
    lbl = [CCLabelTTF labelWithString:text fontName:fontName fontSize:fontSize];
    [lbl setContentSizeInPoints:self.contentSize];
    [self addChild:lbl];
//    [lbl setAnchorPoint:ccp(0.0,0.0)];
//    [lbl setPositionInPoints:ccp(0, self.contentSize.height/2)];
    
    [self updateImage_withImageNamed:glyphName];
}

- (void) updateImage_withImageNamed: (NSString*) imgName {
    if (imgS) {
        [imgS removeFromParent];
        imgS = nil;
    }
    
    if (![imgName isEqual:@""]) {
        imgS = [CCSprite spriteWithImageNamed:imgName];
        [imgS setScale:lbl.fontSize-5/imgS.contentSize.height];
//        [imgS setPositionInPoints:ccp(0, self.contentSize.height/2)];
        [self addChild:imgS];
        [self updateImageScale];
    }
}

- (CCLabelTTF*) textLabel {
    return lbl;
}

- (void) setColorRGBA:(CCColor *)colorRGBA {
    [lbl setColorRGBA:colorRGBA];
}

- (void)updateImageScale {
    if (imgS) {
        //Update Scale of Image everytime the height of the font have changed
        if (imgS.scale != lbl.fontSize/imgS.contentSize.height)
            [imgS setScale:lbl.fontSize/imgS.contentSize.height];
    }
}

- (void) update:(CCTime)delta {
    [self updateImageScale];
}

@end
