//
//  CEMaskedContainer.h
//  MaskingBug
//
//  Created by Paul Hammer on 30.06.16.
//  Copyright © 2016 Paul Willy Hammer. All rights reserved.
//

#import "CCRenderTexture.h"
#import "cocos2d.h"
#import "CCTextureCache.h"

@interface CEMaskedContainer : CCRenderTexture {
    CCTexture *mask;
}

+ (instancetype) containerWithWidth:(float)w height:(float)h;
- (void) updatePosition;
- (void) setMask: (CCTexture*) _m;
- (CCTexture*) createTransparencyTextureMaskWithSize: (CGSize) sz fadeEffectOnBottom: (BOOL) onBottom onTop: (BOOL) onTop;

@end
