//
//  CCTextureCache+TextureCacheExtension.h
//  VERTEX
//
//  Created by Paul Hammer on 23.03.16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCTextureCache.h"

@interface CCTextureCache (TextureCacheExtension)

- (CCTexture*) setTexturWithUImage: (UIImage*) img andKey: (NSString*) key;

@end
